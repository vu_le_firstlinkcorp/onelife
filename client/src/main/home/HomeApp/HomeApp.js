import React, { Component } from 'react';
import './HomeApp.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Container, Row, Col, Progress
  } from 'reactstrap';

class HomeApp extends Component {
  render() {
    return (
        <div>
            <Container fluid>
                <Row>
                    <div className="blankTop"></div>
                    <Col xs="1"></Col>
                    <Col xs="3">
                        <Card>
                            <CardImg top width="100%" src="/gtd_product.jpg" alt="Card image cap" />
                            <CardBody>
                                <CardTitle className="text-success text-center font-weight-bold">GTD APP</CardTitle>
                                {/* <CardSubtitle>Simulator book "Getting thing done"</CardSubtitle> */}
                                <div className="text-center">5%</div>
                                <Progress value="5" />
                                <CardText>David Allen’s Getting Things Done® work-life management system is a global productivity movement that helps people bring order to the chaos in their businesses and their lives.</CardText>
                                <Button color="primary"><Link className="text-white" to="/home">Open app</Link></Button>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col xs="3">
                        <Card>
                            <CardImg top width="100%" src="/coming-soon.jpg" alt="Card image cap" />
                            <CardBody>
                                <CardTitle className="text-success text-center font-weight-bold">Relationship System</CardTitle>
                                <div className="text-center">0%</div>
                                <Progress value="0" />
                                <CardText>could be done someday, or never.</CardText>
                                <Button color="secondary">Coming soon</Button>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col xs="3">
                        <Card>
                            <CardImg top width="100%" src="/coming-soon.jpg" alt="Card image cap" />
                            <CardBody>
                                <CardTitle className="text-success text-center font-weight-bold">Finance</CardTitle>
                                <div className="text-center">0%</div>
                                <Progress value="0" />
                                <CardText>could be done someday, or never.</CardText>
                                <Button color="secondary">Coming soon</Button>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col xs="1"></Col>
                </Row> 
            </Container>
        </div>
    );
  }
}

export default HomeApp;