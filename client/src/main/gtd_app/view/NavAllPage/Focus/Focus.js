import React, { Component } from 'react';
import AppNavbar from '../../../layout/nav/AppNavBar';
import './Focus.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../../layout/header/Header';

class Focus extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Focus page</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Focus;