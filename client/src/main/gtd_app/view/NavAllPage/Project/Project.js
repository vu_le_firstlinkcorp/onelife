import React, { Component } from 'react';
import AppNavbar from '../../../layout/nav/AppNavBar';
import './Project.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../../layout/header/Header';

class Project extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Project page</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Project;