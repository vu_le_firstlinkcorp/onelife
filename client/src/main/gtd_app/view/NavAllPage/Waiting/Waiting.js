import React, { Component } from 'react';
import AppNavbar from '../../../layout/nav/AppNavBar';
import './Waiting.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../../layout/header/Header';

class Waiting extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Waiting page</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Waiting;