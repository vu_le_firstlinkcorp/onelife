import React, { Component } from 'react';
import AppNavbar from '../../../layout/nav/AppNavBar';
import './Next.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../../layout/header/Header';

class Next extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Next page</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Next;