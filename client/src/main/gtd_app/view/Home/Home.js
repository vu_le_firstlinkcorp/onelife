import React, { Component } from 'react';
import './Home.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from '../../layout/nav/AppNavBar';
import { Link } from 'react-router-dom';
import { Button, Container, Row, Col } from 'reactstrap';
import Header from '../../layout/header/Header';

class Home extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Row>
          <AppNavbar/>
          <Col xs="10">
            <Container fluid>
              <Button color="link"><Link to="/">Home Page</Link></Button>
            </Container>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Home;