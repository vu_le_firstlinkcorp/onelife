import React, { Component } from 'react';
import AppNavbar from '../../../../layout/nav/AppNavBar';
import './Area.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../../../layout/header/Header';

class Area extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Area page</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Area;