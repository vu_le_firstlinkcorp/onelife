import React, { Component } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Row, Col, NavItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Header.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {isOpen: false};
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <Row>
      <Col xs="12">
        <Navbar color="dark" dark expand="md">
          <NavbarBrand tag={Link} to="/home">OneLife</NavbarBrand>
          <NavbarToggler onClick={this.toggle}/>
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  setting
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/tag">tag</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/area">area</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/contact">contact</NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  entities
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/items">Items</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/areas">Areas</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/contacts">Contacts</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/feedbacks">Feedbacks</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/projects">Projects</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/references">References</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/tags">Tags</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink className="text-muted" tag={Link} to="/apps">App</NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem>
                <NavLink tag={Link} to="/feedback">
                  feedback
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/version">
                  version
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </Col>
    </Row>;
  }
}