import React, { Component } from 'react';
import { Collapse, Nav, Navbar, NavbarToggler, NavItem, NavLink, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import './AppNavBar.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInbox, faChevronCircleRight, faUserClock, faCalendarAlt, faRecycle, faStar, faSitemap, faBookmark, faBookReader, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

export default class AppNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {isOpen: false};
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <Col xs="2">
      <Navbar id="navBarDisplayHandle" color="light" light expand="md">
        <Col xs="12">
          <NavbarToggler onClick={this.toggle}/>
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav navbar vertical>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
              <p className="text-success text-center font-weight-bold navBarRemoveBottomBlank">-- Capture --</p>
              <NavItem >
                <NavLink tag={Link} to="/inbox">
                  <FontAwesomeIcon icon={faInbox} className="text-success" /> Inbox
                </NavLink>
              </NavItem>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
              <p className="text-success text-center font-weight-bold navBarRemoveBottomBlank">-- Action --</p>
              <NavItem>
                <NavLink tag={Link} to="/next">
                  <FontAwesomeIcon icon={faChevronCircleRight} className="text-success" /> Next
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/waiting">
                  <FontAwesomeIcon icon={faUserClock} className="text-success" /> Waiting
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/scheduled">
                  <FontAwesomeIcon icon={faCalendarAlt} className="text-success" /> Scheduled
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/someday">
                  <FontAwesomeIcon icon={faRecycle} className="text-success" /> Someday
                </NavLink>
              </NavItem>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/focus">
                  <FontAwesomeIcon icon={faStar} className="text-success" /> Focus
                </NavLink>
              </NavItem>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/project">
                  <FontAwesomeIcon icon={faSitemap} className="text-success" /> Project
                </NavLink>
              </NavItem>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/reference">
                  <FontAwesomeIcon icon={faBookmark} className="text-success" /> Reference
                </NavLink>
              </NavItem>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
              <p className="text-success text-center font-weight-bold navBarRemoveBottomBlank">-- Clean up --</p>
              <NavItem>
                <NavLink tag={Link} to="/logbook">
                  <FontAwesomeIcon icon={faBookReader} className="text-success" /> Logbook
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/trash">
                  <FontAwesomeIcon icon={faTrashAlt} className="text-success" /> Trash
                </NavLink>
              </NavItem>
              <NavItem>
                <hr className="navBarHandleTopBottom borderHrSuccess"/>
              </NavItem>
            </Nav>
          </Collapse>
        </Col>
      </Navbar>
    </Col>;
  }
}