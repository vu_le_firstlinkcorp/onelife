import React, { Component } from 'react';
import AppNavbar from '../../layout/nav/AppNavBar';
import './Apps.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../layout/header/Header';

class Apps extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Apps Entities</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Apps;