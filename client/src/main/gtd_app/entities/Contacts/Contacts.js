import React, { Component } from 'react';
import AppNavbar from '../../layout/nav/AppNavBar';
import './Contacts.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../layout/header/Header';

class Contacts extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Contacts Entities</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Contacts;