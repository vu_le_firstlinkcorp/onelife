import React, { Component } from 'react';
import AppNavbar from '../../layout/nav/AppNavBar';
import './Tags.css';
import { Container, Row, Col } from 'reactstrap';
import Header from '../../layout/header/Header';

class Tags extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Row>
                <AppNavbar/>
                <Col xs="10">
                    <Container fluid>
                        <h1>Tags Entities</h1>
                    </Container>
                </Col>
                </Row>
            </div>
        )
    }
}

export default Tags;