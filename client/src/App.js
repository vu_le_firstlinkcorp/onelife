import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomeApp from './main/home/HomeApp/HomeApp';

/* all import for navBar left page */
import Home from './main/gtd_app/view/Home/Home';
import Inbox from './main/gtd_app/view/NavAllPage/Inbox/Inbox';
import Next from './main/gtd_app/view/NavAllPage/Next/Next';
import Waiting from './main/gtd_app/view/NavAllPage/Waiting/Waiting';
import Scheduled from './main/gtd_app/view/NavAllPage/Scheduled/Scheduled';
import Someday from './main/gtd_app/view/NavAllPage/Someday/Someday';
import Focus from './main/gtd_app/view/NavAllPage/Focus/Focus';
import Project from './main/gtd_app/view/NavAllPage/Project/Project';
import Reference from './main/gtd_app/view/NavAllPage/Reference/Reference';
import Logbook from './main/gtd_app/view/NavAllPage/Logbook/Logbook';
import Trash from './main/gtd_app/view/NavAllPage/Trash/Trash';

/* all import for header page */
import Area from './main/gtd_app/view/HeaderAllPage/Setting/Area/Area';
import Contact from './main/gtd_app/view/HeaderAllPage/Setting/Contact/Contact';
import Tag from './main/gtd_app/view/HeaderAllPage/Setting/Tag/Tag';
import Feedback from './main/gtd_app/view/HeaderAllPage/Feedback/Feedback';
import Version from './main/gtd_app/view/HeaderAllPage/Version/Version';

/* all import for entities page */
import Items from './main/gtd_app/entities/Items/Items';
import Areas from './main/gtd_app/entities/Areas/Areas';
import Contacts from './main/gtd_app/entities/Contacts/Contacts';
import Feedbacks from './main/gtd_app/entities/Feedbacks/Feedbacks';
import Projects from './main/gtd_app/entities/Projects/Projects';
import References from './main/gtd_app/entities/References/References';
import Tags from './main/gtd_app/entities/Tags/Tags';
import Apps from './main/gtd_app/entities/Apps/Apps';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={HomeApp}/>

          {/* all page of navBar left */}
          <Route path='/home' exact={true} component={Home}/>
          <Route path='/inbox' exact={true} component={Inbox}/>
          <Route path='/next' exact={true} component={Next}/>
          <Route path='/waiting' exact={true} component={Waiting}/>
          <Route path='/scheduled' exact={true} component={Scheduled}/>
          <Route path='/someday' exact={true} component={Someday}/>
          <Route path='/focus' exact={true} component={Focus}/>
          <Route path='/project' exact={true} component={Project}/>
          <Route path='/reference' exact={true} component={Reference}/>
          <Route path='/logbook' exact={true} component={Logbook}/>
          <Route path='/trash' exact={true} component={Trash}/>

          {/* all page of Header */}
          <Route path='/area' exact={true} component={Area}/>
          <Route path='/contact' exact={true} component={Contact}/>
          <Route path='/tag' exact={true} component={Tag}/>
          <Route path='/feedback' exact={true} component={Feedback}/>
          <Route path='/version' exact={true} component={Version}/>

          {/* all page of Entities */}
          <Route path='/items' exact={true} component={Items}/>
          <Route path='/areas' exact={true} component={Areas}/>
          <Route path='/contacts' exact={true} component={Contacts}/>
          <Route path='/feedbacks' exact={true} component={Feedbacks}/>
          <Route path='/projects' exact={true} component={Projects}/>
          <Route path='/references' exact={true} component={References}/>
          <Route path='/tags' exact={true} component={Tags}/>
          <Route path='/apps' exact={true} component={Apps}/>
        </Switch>
      </Router>
    )
  }
}

export default App;