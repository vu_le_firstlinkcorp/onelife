package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.Items;
import com.onelife.gtd.domain.Contacts;
import com.onelife.gtd.domain.Areas;
import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.domain.Tags;
import com.onelife.gtd.domain.References;
import com.onelife.gtd.repository.ItemsRepository;
import com.onelife.gtd.service.ItemsService;
import com.onelife.gtd.service.dto.ItemsDTO;
import com.onelife.gtd.service.mapper.ItemsMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.ItemsCriteria;
import com.onelife.gtd.service.ItemsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ItemsResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class ItemsResourceIT {

    private static final String DEFAULT_TO_DO = "AAAAAAAAAA";
    private static final String UPDATED_TO_DO = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final String DEFAULT_TIME = "AAAAAAAAAA";
    private static final String UPDATED_TIME = "BBBBBBBBBB";

    private static final String DEFAULT_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_ENERGY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DUE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DUE_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_ITEM_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_FOCUS = "AAAAAAAAAA";
    private static final String UPDATED_FOCUS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    @Autowired
    private ItemsRepository itemsRepository;

    @Mock
    private ItemsRepository itemsRepositoryMock;

    @Autowired
    private ItemsMapper itemsMapper;

    @Mock
    private ItemsService itemsServiceMock;

    @Autowired
    private ItemsService itemsService;

    @Autowired
    private ItemsQueryService itemsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restItemsMockMvc;

    private Items items;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ItemsResource itemsResource = new ItemsResource(itemsService, itemsQueryService);
        this.restItemsMockMvc = MockMvcBuilders.standaloneSetup(itemsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Items createEntity(EntityManager em) {
        Items items = new Items()
            .toDo(DEFAULT_TO_DO)
            .notes(DEFAULT_NOTES)
            .time(DEFAULT_TIME)
            .energy(DEFAULT_ENERGY)
            .dueDate(DEFAULT_DUE_DATE)
            .itemType(DEFAULT_ITEM_TYPE)
            .focus(DEFAULT_FOCUS)
            .status(DEFAULT_STATUS);
        return items;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Items createUpdatedEntity(EntityManager em) {
        Items items = new Items()
            .toDo(UPDATED_TO_DO)
            .notes(UPDATED_NOTES)
            .time(UPDATED_TIME)
            .energy(UPDATED_ENERGY)
            .dueDate(UPDATED_DUE_DATE)
            .itemType(UPDATED_ITEM_TYPE)
            .focus(UPDATED_FOCUS)
            .status(UPDATED_STATUS);
        return items;
    }

    @BeforeEach
    public void initTest() {
        items = createEntity(em);
    }

    @Test
    @Transactional
    public void createItems() throws Exception {
        int databaseSizeBeforeCreate = itemsRepository.findAll().size();

        // Create the Items
        ItemsDTO itemsDTO = itemsMapper.toDto(items);
        restItemsMockMvc.perform(post("/api/items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itemsDTO)))
            .andExpect(status().isCreated());

        // Validate the Items in the database
        List<Items> itemsList = itemsRepository.findAll();
        assertThat(itemsList).hasSize(databaseSizeBeforeCreate + 1);
        Items testItems = itemsList.get(itemsList.size() - 1);
        assertThat(testItems.getToDo()).isEqualTo(DEFAULT_TO_DO);
        assertThat(testItems.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testItems.getTime()).isEqualTo(DEFAULT_TIME);
        assertThat(testItems.getEnergy()).isEqualTo(DEFAULT_ENERGY);
        assertThat(testItems.getDueDate()).isEqualTo(DEFAULT_DUE_DATE);
        assertThat(testItems.getItemType()).isEqualTo(DEFAULT_ITEM_TYPE);
        assertThat(testItems.getFocus()).isEqualTo(DEFAULT_FOCUS);
        assertThat(testItems.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createItemsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = itemsRepository.findAll().size();

        // Create the Items with an existing ID
        items.setId(1L);
        ItemsDTO itemsDTO = itemsMapper.toDto(items);

        // An entity with an existing ID cannot be created, so this API call must fail
        restItemsMockMvc.perform(post("/api/items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itemsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Items in the database
        List<Items> itemsList = itemsRepository.findAll();
        assertThat(itemsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllItems() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList
        restItemsMockMvc.perform(get("/api/items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(items.getId().intValue())))
            .andExpect(jsonPath("$.[*].toDo").value(hasItem(DEFAULT_TO_DO)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME)))
            .andExpect(jsonPath("$.[*].energy").value(hasItem(DEFAULT_ENERGY)))
            .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].itemType").value(hasItem(DEFAULT_ITEM_TYPE)))
            .andExpect(jsonPath("$.[*].focus").value(hasItem(DEFAULT_FOCUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllItemsWithEagerRelationshipsIsEnabled() throws Exception {
        ItemsResource itemsResource = new ItemsResource(itemsServiceMock, itemsQueryService);
        when(itemsServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restItemsMockMvc = MockMvcBuilders.standaloneSetup(itemsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restItemsMockMvc.perform(get("/api/items?eagerload=true"))
        .andExpect(status().isOk());

        verify(itemsServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllItemsWithEagerRelationshipsIsNotEnabled() throws Exception {
        ItemsResource itemsResource = new ItemsResource(itemsServiceMock, itemsQueryService);
            when(itemsServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restItemsMockMvc = MockMvcBuilders.standaloneSetup(itemsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restItemsMockMvc.perform(get("/api/items?eagerload=true"))
        .andExpect(status().isOk());

            verify(itemsServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getItems() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get the items
        restItemsMockMvc.perform(get("/api/items/{id}", items.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(items.getId().intValue()))
            .andExpect(jsonPath("$.toDo").value(DEFAULT_TO_DO))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES))
            .andExpect(jsonPath("$.time").value(DEFAULT_TIME))
            .andExpect(jsonPath("$.energy").value(DEFAULT_ENERGY))
            .andExpect(jsonPath("$.dueDate").value(DEFAULT_DUE_DATE.toString()))
            .andExpect(jsonPath("$.itemType").value(DEFAULT_ITEM_TYPE))
            .andExpect(jsonPath("$.focus").value(DEFAULT_FOCUS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getAllItemsByToDoIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where toDo equals to DEFAULT_TO_DO
        defaultItemsShouldBeFound("toDo.equals=" + DEFAULT_TO_DO);

        // Get all the itemsList where toDo equals to UPDATED_TO_DO
        defaultItemsShouldNotBeFound("toDo.equals=" + UPDATED_TO_DO);
    }

    @Test
    @Transactional
    public void getAllItemsByToDoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where toDo not equals to DEFAULT_TO_DO
        defaultItemsShouldNotBeFound("toDo.notEquals=" + DEFAULT_TO_DO);

        // Get all the itemsList where toDo not equals to UPDATED_TO_DO
        defaultItemsShouldBeFound("toDo.notEquals=" + UPDATED_TO_DO);
    }

    @Test
    @Transactional
    public void getAllItemsByToDoIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where toDo in DEFAULT_TO_DO or UPDATED_TO_DO
        defaultItemsShouldBeFound("toDo.in=" + DEFAULT_TO_DO + "," + UPDATED_TO_DO);

        // Get all the itemsList where toDo equals to UPDATED_TO_DO
        defaultItemsShouldNotBeFound("toDo.in=" + UPDATED_TO_DO);
    }

    @Test
    @Transactional
    public void getAllItemsByToDoIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where toDo is not null
        defaultItemsShouldBeFound("toDo.specified=true");

        // Get all the itemsList where toDo is null
        defaultItemsShouldNotBeFound("toDo.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByToDoContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where toDo contains DEFAULT_TO_DO
        defaultItemsShouldBeFound("toDo.contains=" + DEFAULT_TO_DO);

        // Get all the itemsList where toDo contains UPDATED_TO_DO
        defaultItemsShouldNotBeFound("toDo.contains=" + UPDATED_TO_DO);
    }

    @Test
    @Transactional
    public void getAllItemsByToDoNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where toDo does not contain DEFAULT_TO_DO
        defaultItemsShouldNotBeFound("toDo.doesNotContain=" + DEFAULT_TO_DO);

        // Get all the itemsList where toDo does not contain UPDATED_TO_DO
        defaultItemsShouldBeFound("toDo.doesNotContain=" + UPDATED_TO_DO);
    }


    @Test
    @Transactional
    public void getAllItemsByNotesIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where notes equals to DEFAULT_NOTES
        defaultItemsShouldBeFound("notes.equals=" + DEFAULT_NOTES);

        // Get all the itemsList where notes equals to UPDATED_NOTES
        defaultItemsShouldNotBeFound("notes.equals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllItemsByNotesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where notes not equals to DEFAULT_NOTES
        defaultItemsShouldNotBeFound("notes.notEquals=" + DEFAULT_NOTES);

        // Get all the itemsList where notes not equals to UPDATED_NOTES
        defaultItemsShouldBeFound("notes.notEquals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllItemsByNotesIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where notes in DEFAULT_NOTES or UPDATED_NOTES
        defaultItemsShouldBeFound("notes.in=" + DEFAULT_NOTES + "," + UPDATED_NOTES);

        // Get all the itemsList where notes equals to UPDATED_NOTES
        defaultItemsShouldNotBeFound("notes.in=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllItemsByNotesIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where notes is not null
        defaultItemsShouldBeFound("notes.specified=true");

        // Get all the itemsList where notes is null
        defaultItemsShouldNotBeFound("notes.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByNotesContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where notes contains DEFAULT_NOTES
        defaultItemsShouldBeFound("notes.contains=" + DEFAULT_NOTES);

        // Get all the itemsList where notes contains UPDATED_NOTES
        defaultItemsShouldNotBeFound("notes.contains=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllItemsByNotesNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where notes does not contain DEFAULT_NOTES
        defaultItemsShouldNotBeFound("notes.doesNotContain=" + DEFAULT_NOTES);

        // Get all the itemsList where notes does not contain UPDATED_NOTES
        defaultItemsShouldBeFound("notes.doesNotContain=" + UPDATED_NOTES);
    }


    @Test
    @Transactional
    public void getAllItemsByTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where time equals to DEFAULT_TIME
        defaultItemsShouldBeFound("time.equals=" + DEFAULT_TIME);

        // Get all the itemsList where time equals to UPDATED_TIME
        defaultItemsShouldNotBeFound("time.equals=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllItemsByTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where time not equals to DEFAULT_TIME
        defaultItemsShouldNotBeFound("time.notEquals=" + DEFAULT_TIME);

        // Get all the itemsList where time not equals to UPDATED_TIME
        defaultItemsShouldBeFound("time.notEquals=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllItemsByTimeIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where time in DEFAULT_TIME or UPDATED_TIME
        defaultItemsShouldBeFound("time.in=" + DEFAULT_TIME + "," + UPDATED_TIME);

        // Get all the itemsList where time equals to UPDATED_TIME
        defaultItemsShouldNotBeFound("time.in=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllItemsByTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where time is not null
        defaultItemsShouldBeFound("time.specified=true");

        // Get all the itemsList where time is null
        defaultItemsShouldNotBeFound("time.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByTimeContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where time contains DEFAULT_TIME
        defaultItemsShouldBeFound("time.contains=" + DEFAULT_TIME);

        // Get all the itemsList where time contains UPDATED_TIME
        defaultItemsShouldNotBeFound("time.contains=" + UPDATED_TIME);
    }

    @Test
    @Transactional
    public void getAllItemsByTimeNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where time does not contain DEFAULT_TIME
        defaultItemsShouldNotBeFound("time.doesNotContain=" + DEFAULT_TIME);

        // Get all the itemsList where time does not contain UPDATED_TIME
        defaultItemsShouldBeFound("time.doesNotContain=" + UPDATED_TIME);
    }


    @Test
    @Transactional
    public void getAllItemsByEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where energy equals to DEFAULT_ENERGY
        defaultItemsShouldBeFound("energy.equals=" + DEFAULT_ENERGY);

        // Get all the itemsList where energy equals to UPDATED_ENERGY
        defaultItemsShouldNotBeFound("energy.equals=" + UPDATED_ENERGY);
    }

    @Test
    @Transactional
    public void getAllItemsByEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where energy not equals to DEFAULT_ENERGY
        defaultItemsShouldNotBeFound("energy.notEquals=" + DEFAULT_ENERGY);

        // Get all the itemsList where energy not equals to UPDATED_ENERGY
        defaultItemsShouldBeFound("energy.notEquals=" + UPDATED_ENERGY);
    }

    @Test
    @Transactional
    public void getAllItemsByEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where energy in DEFAULT_ENERGY or UPDATED_ENERGY
        defaultItemsShouldBeFound("energy.in=" + DEFAULT_ENERGY + "," + UPDATED_ENERGY);

        // Get all the itemsList where energy equals to UPDATED_ENERGY
        defaultItemsShouldNotBeFound("energy.in=" + UPDATED_ENERGY);
    }

    @Test
    @Transactional
    public void getAllItemsByEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where energy is not null
        defaultItemsShouldBeFound("energy.specified=true");

        // Get all the itemsList where energy is null
        defaultItemsShouldNotBeFound("energy.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByEnergyContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where energy contains DEFAULT_ENERGY
        defaultItemsShouldBeFound("energy.contains=" + DEFAULT_ENERGY);

        // Get all the itemsList where energy contains UPDATED_ENERGY
        defaultItemsShouldNotBeFound("energy.contains=" + UPDATED_ENERGY);
    }

    @Test
    @Transactional
    public void getAllItemsByEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where energy does not contain DEFAULT_ENERGY
        defaultItemsShouldNotBeFound("energy.doesNotContain=" + DEFAULT_ENERGY);

        // Get all the itemsList where energy does not contain UPDATED_ENERGY
        defaultItemsShouldBeFound("energy.doesNotContain=" + UPDATED_ENERGY);
    }


    @Test
    @Transactional
    public void getAllItemsByDueDateIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate equals to DEFAULT_DUE_DATE
        defaultItemsShouldBeFound("dueDate.equals=" + DEFAULT_DUE_DATE);

        // Get all the itemsList where dueDate equals to UPDATED_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.equals=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate not equals to DEFAULT_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.notEquals=" + DEFAULT_DUE_DATE);

        // Get all the itemsList where dueDate not equals to UPDATED_DUE_DATE
        defaultItemsShouldBeFound("dueDate.notEquals=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate in DEFAULT_DUE_DATE or UPDATED_DUE_DATE
        defaultItemsShouldBeFound("dueDate.in=" + DEFAULT_DUE_DATE + "," + UPDATED_DUE_DATE);

        // Get all the itemsList where dueDate equals to UPDATED_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.in=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate is not null
        defaultItemsShouldBeFound("dueDate.specified=true");

        // Get all the itemsList where dueDate is null
        defaultItemsShouldNotBeFound("dueDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate is greater than or equal to DEFAULT_DUE_DATE
        defaultItemsShouldBeFound("dueDate.greaterThanOrEqual=" + DEFAULT_DUE_DATE);

        // Get all the itemsList where dueDate is greater than or equal to UPDATED_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.greaterThanOrEqual=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate is less than or equal to DEFAULT_DUE_DATE
        defaultItemsShouldBeFound("dueDate.lessThanOrEqual=" + DEFAULT_DUE_DATE);

        // Get all the itemsList where dueDate is less than or equal to SMALLER_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.lessThanOrEqual=" + SMALLER_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsLessThanSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate is less than DEFAULT_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.lessThan=" + DEFAULT_DUE_DATE);

        // Get all the itemsList where dueDate is less than UPDATED_DUE_DATE
        defaultItemsShouldBeFound("dueDate.lessThan=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllItemsByDueDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where dueDate is greater than DEFAULT_DUE_DATE
        defaultItemsShouldNotBeFound("dueDate.greaterThan=" + DEFAULT_DUE_DATE);

        // Get all the itemsList where dueDate is greater than SMALLER_DUE_DATE
        defaultItemsShouldBeFound("dueDate.greaterThan=" + SMALLER_DUE_DATE);
    }


    @Test
    @Transactional
    public void getAllItemsByItemTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where itemType equals to DEFAULT_ITEM_TYPE
        defaultItemsShouldBeFound("itemType.equals=" + DEFAULT_ITEM_TYPE);

        // Get all the itemsList where itemType equals to UPDATED_ITEM_TYPE
        defaultItemsShouldNotBeFound("itemType.equals=" + UPDATED_ITEM_TYPE);
    }

    @Test
    @Transactional
    public void getAllItemsByItemTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where itemType not equals to DEFAULT_ITEM_TYPE
        defaultItemsShouldNotBeFound("itemType.notEquals=" + DEFAULT_ITEM_TYPE);

        // Get all the itemsList where itemType not equals to UPDATED_ITEM_TYPE
        defaultItemsShouldBeFound("itemType.notEquals=" + UPDATED_ITEM_TYPE);
    }

    @Test
    @Transactional
    public void getAllItemsByItemTypeIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where itemType in DEFAULT_ITEM_TYPE or UPDATED_ITEM_TYPE
        defaultItemsShouldBeFound("itemType.in=" + DEFAULT_ITEM_TYPE + "," + UPDATED_ITEM_TYPE);

        // Get all the itemsList where itemType equals to UPDATED_ITEM_TYPE
        defaultItemsShouldNotBeFound("itemType.in=" + UPDATED_ITEM_TYPE);
    }

    @Test
    @Transactional
    public void getAllItemsByItemTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where itemType is not null
        defaultItemsShouldBeFound("itemType.specified=true");

        // Get all the itemsList where itemType is null
        defaultItemsShouldNotBeFound("itemType.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByItemTypeContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where itemType contains DEFAULT_ITEM_TYPE
        defaultItemsShouldBeFound("itemType.contains=" + DEFAULT_ITEM_TYPE);

        // Get all the itemsList where itemType contains UPDATED_ITEM_TYPE
        defaultItemsShouldNotBeFound("itemType.contains=" + UPDATED_ITEM_TYPE);
    }

    @Test
    @Transactional
    public void getAllItemsByItemTypeNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where itemType does not contain DEFAULT_ITEM_TYPE
        defaultItemsShouldNotBeFound("itemType.doesNotContain=" + DEFAULT_ITEM_TYPE);

        // Get all the itemsList where itemType does not contain UPDATED_ITEM_TYPE
        defaultItemsShouldBeFound("itemType.doesNotContain=" + UPDATED_ITEM_TYPE);
    }


    @Test
    @Transactional
    public void getAllItemsByFocusIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where focus equals to DEFAULT_FOCUS
        defaultItemsShouldBeFound("focus.equals=" + DEFAULT_FOCUS);

        // Get all the itemsList where focus equals to UPDATED_FOCUS
        defaultItemsShouldNotBeFound("focus.equals=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllItemsByFocusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where focus not equals to DEFAULT_FOCUS
        defaultItemsShouldNotBeFound("focus.notEquals=" + DEFAULT_FOCUS);

        // Get all the itemsList where focus not equals to UPDATED_FOCUS
        defaultItemsShouldBeFound("focus.notEquals=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllItemsByFocusIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where focus in DEFAULT_FOCUS or UPDATED_FOCUS
        defaultItemsShouldBeFound("focus.in=" + DEFAULT_FOCUS + "," + UPDATED_FOCUS);

        // Get all the itemsList where focus equals to UPDATED_FOCUS
        defaultItemsShouldNotBeFound("focus.in=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllItemsByFocusIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where focus is not null
        defaultItemsShouldBeFound("focus.specified=true");

        // Get all the itemsList where focus is null
        defaultItemsShouldNotBeFound("focus.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByFocusContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where focus contains DEFAULT_FOCUS
        defaultItemsShouldBeFound("focus.contains=" + DEFAULT_FOCUS);

        // Get all the itemsList where focus contains UPDATED_FOCUS
        defaultItemsShouldNotBeFound("focus.contains=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllItemsByFocusNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where focus does not contain DEFAULT_FOCUS
        defaultItemsShouldNotBeFound("focus.doesNotContain=" + DEFAULT_FOCUS);

        // Get all the itemsList where focus does not contain UPDATED_FOCUS
        defaultItemsShouldBeFound("focus.doesNotContain=" + UPDATED_FOCUS);
    }


    @Test
    @Transactional
    public void getAllItemsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where status equals to DEFAULT_STATUS
        defaultItemsShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the itemsList where status equals to UPDATED_STATUS
        defaultItemsShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllItemsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where status not equals to DEFAULT_STATUS
        defaultItemsShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the itemsList where status not equals to UPDATED_STATUS
        defaultItemsShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllItemsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultItemsShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the itemsList where status equals to UPDATED_STATUS
        defaultItemsShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllItemsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where status is not null
        defaultItemsShouldBeFound("status.specified=true");

        // Get all the itemsList where status is null
        defaultItemsShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllItemsByStatusContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where status contains DEFAULT_STATUS
        defaultItemsShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the itemsList where status contains UPDATED_STATUS
        defaultItemsShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllItemsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        // Get all the itemsList where status does not contain DEFAULT_STATUS
        defaultItemsShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the itemsList where status does not contain UPDATED_STATUS
        defaultItemsShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllItemsByContactsIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);
        Contacts contacts = ContactsResourceIT.createEntity(em);
        em.persist(contacts);
        em.flush();
        items.addContacts(contacts);
        itemsRepository.saveAndFlush(items);
        Long contactsId = contacts.getId();

        // Get all the itemsList where contacts equals to contactsId
        defaultItemsShouldBeFound("contactsId.equals=" + contactsId);

        // Get all the itemsList where contacts equals to contactsId + 1
        defaultItemsShouldNotBeFound("contactsId.equals=" + (contactsId + 1));
    }


    @Test
    @Transactional
    public void getAllItemsByAreasIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);
        Areas areas = AreasResourceIT.createEntity(em);
        em.persist(areas);
        em.flush();
        items.addAreas(areas);
        itemsRepository.saveAndFlush(items);
        Long areasId = areas.getId();

        // Get all the itemsList where areas equals to areasId
        defaultItemsShouldBeFound("areasId.equals=" + areasId);

        // Get all the itemsList where areas equals to areasId + 1
        defaultItemsShouldNotBeFound("areasId.equals=" + (areasId + 1));
    }


    @Test
    @Transactional
    public void getAllItemsByProjecsIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);
        Projecs projecs = ProjecsResourceIT.createEntity(em);
        em.persist(projecs);
        em.flush();
        items.addProjecs(projecs);
        itemsRepository.saveAndFlush(items);
        Long projecsId = projecs.getId();

        // Get all the itemsList where projecs equals to projecsId
        defaultItemsShouldBeFound("projecsId.equals=" + projecsId);

        // Get all the itemsList where projecs equals to projecsId + 1
        defaultItemsShouldNotBeFound("projecsId.equals=" + (projecsId + 1));
    }


    @Test
    @Transactional
    public void getAllItemsByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);
        Tags tags = TagsResourceIT.createEntity(em);
        em.persist(tags);
        em.flush();
        items.addTags(tags);
        itemsRepository.saveAndFlush(items);
        Long tagsId = tags.getId();

        // Get all the itemsList where tags equals to tagsId
        defaultItemsShouldBeFound("tagsId.equals=" + tagsId);

        // Get all the itemsList where tags equals to tagsId + 1
        defaultItemsShouldNotBeFound("tagsId.equals=" + (tagsId + 1));
    }


    @Test
    @Transactional
    public void getAllItemsByReferencesIsEqualToSomething() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);
        References references = ReferencesResourceIT.createEntity(em);
        em.persist(references);
        em.flush();
        items.addReferences(references);
        itemsRepository.saveAndFlush(items);
        Long referencesId = references.getId();

        // Get all the itemsList where references equals to referencesId
        defaultItemsShouldBeFound("referencesId.equals=" + referencesId);

        // Get all the itemsList where references equals to referencesId + 1
        defaultItemsShouldNotBeFound("referencesId.equals=" + (referencesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultItemsShouldBeFound(String filter) throws Exception {
        restItemsMockMvc.perform(get("/api/items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(items.getId().intValue())))
            .andExpect(jsonPath("$.[*].toDo").value(hasItem(DEFAULT_TO_DO)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME)))
            .andExpect(jsonPath("$.[*].energy").value(hasItem(DEFAULT_ENERGY)))
            .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].itemType").value(hasItem(DEFAULT_ITEM_TYPE)))
            .andExpect(jsonPath("$.[*].focus").value(hasItem(DEFAULT_FOCUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));

        // Check, that the count call also returns 1
        restItemsMockMvc.perform(get("/api/items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultItemsShouldNotBeFound(String filter) throws Exception {
        restItemsMockMvc.perform(get("/api/items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restItemsMockMvc.perform(get("/api/items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingItems() throws Exception {
        // Get the items
        restItemsMockMvc.perform(get("/api/items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateItems() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        int databaseSizeBeforeUpdate = itemsRepository.findAll().size();

        // Update the items
        Items updatedItems = itemsRepository.findById(items.getId()).get();
        // Disconnect from session so that the updates on updatedItems are not directly saved in db
        em.detach(updatedItems);
        updatedItems
            .toDo(UPDATED_TO_DO)
            .notes(UPDATED_NOTES)
            .time(UPDATED_TIME)
            .energy(UPDATED_ENERGY)
            .dueDate(UPDATED_DUE_DATE)
            .itemType(UPDATED_ITEM_TYPE)
            .focus(UPDATED_FOCUS)
            .status(UPDATED_STATUS);
        ItemsDTO itemsDTO = itemsMapper.toDto(updatedItems);

        restItemsMockMvc.perform(put("/api/items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itemsDTO)))
            .andExpect(status().isOk());

        // Validate the Items in the database
        List<Items> itemsList = itemsRepository.findAll();
        assertThat(itemsList).hasSize(databaseSizeBeforeUpdate);
        Items testItems = itemsList.get(itemsList.size() - 1);
        assertThat(testItems.getToDo()).isEqualTo(UPDATED_TO_DO);
        assertThat(testItems.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testItems.getTime()).isEqualTo(UPDATED_TIME);
        assertThat(testItems.getEnergy()).isEqualTo(UPDATED_ENERGY);
        assertThat(testItems.getDueDate()).isEqualTo(UPDATED_DUE_DATE);
        assertThat(testItems.getItemType()).isEqualTo(UPDATED_ITEM_TYPE);
        assertThat(testItems.getFocus()).isEqualTo(UPDATED_FOCUS);
        assertThat(testItems.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingItems() throws Exception {
        int databaseSizeBeforeUpdate = itemsRepository.findAll().size();

        // Create the Items
        ItemsDTO itemsDTO = itemsMapper.toDto(items);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restItemsMockMvc.perform(put("/api/items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(itemsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Items in the database
        List<Items> itemsList = itemsRepository.findAll();
        assertThat(itemsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteItems() throws Exception {
        // Initialize the database
        itemsRepository.saveAndFlush(items);

        int databaseSizeBeforeDelete = itemsRepository.findAll().size();

        // Delete the items
        restItemsMockMvc.perform(delete("/api/items/{id}", items.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Items> itemsList = itemsRepository.findAll();
        assertThat(itemsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Items.class);
        Items items1 = new Items();
        items1.setId(1L);
        Items items2 = new Items();
        items2.setId(items1.getId());
        assertThat(items1).isEqualTo(items2);
        items2.setId(2L);
        assertThat(items1).isNotEqualTo(items2);
        items1.setId(null);
        assertThat(items1).isNotEqualTo(items2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ItemsDTO.class);
        ItemsDTO itemsDTO1 = new ItemsDTO();
        itemsDTO1.setId(1L);
        ItemsDTO itemsDTO2 = new ItemsDTO();
        assertThat(itemsDTO1).isNotEqualTo(itemsDTO2);
        itemsDTO2.setId(itemsDTO1.getId());
        assertThat(itemsDTO1).isEqualTo(itemsDTO2);
        itemsDTO2.setId(2L);
        assertThat(itemsDTO1).isNotEqualTo(itemsDTO2);
        itemsDTO1.setId(null);
        assertThat(itemsDTO1).isNotEqualTo(itemsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(itemsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(itemsMapper.fromId(null)).isNull();
    }
}
