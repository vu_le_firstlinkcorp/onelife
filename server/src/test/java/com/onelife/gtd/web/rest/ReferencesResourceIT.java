package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.References;
import com.onelife.gtd.domain.Items;
import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.repository.ReferencesRepository;
import com.onelife.gtd.service.ReferencesService;
import com.onelife.gtd.service.dto.ReferencesDTO;
import com.onelife.gtd.service.mapper.ReferencesMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.ReferencesCriteria;
import com.onelife.gtd.service.ReferencesQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ReferencesResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class ReferencesResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    @Autowired
    private ReferencesRepository referencesRepository;

    @Autowired
    private ReferencesMapper referencesMapper;

    @Autowired
    private ReferencesService referencesService;

    @Autowired
    private ReferencesQueryService referencesQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restReferencesMockMvc;

    private References references;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReferencesResource referencesResource = new ReferencesResource(referencesService, referencesQueryService);
        this.restReferencesMockMvc = MockMvcBuilders.standaloneSetup(referencesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static References createEntity(EntityManager em) {
        References references = new References()
            .name(DEFAULT_NAME)
            .notes(DEFAULT_NOTES);
        return references;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static References createUpdatedEntity(EntityManager em) {
        References references = new References()
            .name(UPDATED_NAME)
            .notes(UPDATED_NOTES);
        return references;
    }

    @BeforeEach
    public void initTest() {
        references = createEntity(em);
    }

    @Test
    @Transactional
    public void createReferences() throws Exception {
        int databaseSizeBeforeCreate = referencesRepository.findAll().size();

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);
        restReferencesMockMvc.perform(post("/api/references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isCreated());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeCreate + 1);
        References testReferences = referencesList.get(referencesList.size() - 1);
        assertThat(testReferences.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testReferences.getNotes()).isEqualTo(DEFAULT_NOTES);
    }

    @Test
    @Transactional
    public void createReferencesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = referencesRepository.findAll().size();

        // Create the References with an existing ID
        references.setId(1L);
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReferencesMockMvc.perform(post("/api/references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList
        restReferencesMockMvc.perform(get("/api/references?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(references.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)));
    }
    
    @Test
    @Transactional
    public void getReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get the references
        restReferencesMockMvc.perform(get("/api/references/{id}", references.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(references.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES));
    }

    @Test
    @Transactional
    public void getAllReferencesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name equals to DEFAULT_NAME
        defaultReferencesShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the referencesList where name equals to UPDATED_NAME
        defaultReferencesShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllReferencesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name not equals to DEFAULT_NAME
        defaultReferencesShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the referencesList where name not equals to UPDATED_NAME
        defaultReferencesShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllReferencesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name in DEFAULT_NAME or UPDATED_NAME
        defaultReferencesShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the referencesList where name equals to UPDATED_NAME
        defaultReferencesShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllReferencesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name is not null
        defaultReferencesShouldBeFound("name.specified=true");

        // Get all the referencesList where name is null
        defaultReferencesShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllReferencesByNameContainsSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name contains DEFAULT_NAME
        defaultReferencesShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the referencesList where name contains UPDATED_NAME
        defaultReferencesShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllReferencesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name does not contain DEFAULT_NAME
        defaultReferencesShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the referencesList where name does not contain UPDATED_NAME
        defaultReferencesShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllReferencesByNotesIsEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where notes equals to DEFAULT_NOTES
        defaultReferencesShouldBeFound("notes.equals=" + DEFAULT_NOTES);

        // Get all the referencesList where notes equals to UPDATED_NOTES
        defaultReferencesShouldNotBeFound("notes.equals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllReferencesByNotesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where notes not equals to DEFAULT_NOTES
        defaultReferencesShouldNotBeFound("notes.notEquals=" + DEFAULT_NOTES);

        // Get all the referencesList where notes not equals to UPDATED_NOTES
        defaultReferencesShouldBeFound("notes.notEquals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllReferencesByNotesIsInShouldWork() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where notes in DEFAULT_NOTES or UPDATED_NOTES
        defaultReferencesShouldBeFound("notes.in=" + DEFAULT_NOTES + "," + UPDATED_NOTES);

        // Get all the referencesList where notes equals to UPDATED_NOTES
        defaultReferencesShouldNotBeFound("notes.in=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllReferencesByNotesIsNullOrNotNull() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where notes is not null
        defaultReferencesShouldBeFound("notes.specified=true");

        // Get all the referencesList where notes is null
        defaultReferencesShouldNotBeFound("notes.specified=false");
    }
                @Test
    @Transactional
    public void getAllReferencesByNotesContainsSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where notes contains DEFAULT_NOTES
        defaultReferencesShouldBeFound("notes.contains=" + DEFAULT_NOTES);

        // Get all the referencesList where notes contains UPDATED_NOTES
        defaultReferencesShouldNotBeFound("notes.contains=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllReferencesByNotesNotContainsSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where notes does not contain DEFAULT_NOTES
        defaultReferencesShouldNotBeFound("notes.doesNotContain=" + DEFAULT_NOTES);

        // Get all the referencesList where notes does not contain UPDATED_NOTES
        defaultReferencesShouldBeFound("notes.doesNotContain=" + UPDATED_NOTES);
    }


    @Test
    @Transactional
    public void getAllReferencesByItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);
        Items items = ItemsResourceIT.createEntity(em);
        em.persist(items);
        em.flush();
        references.addItems(items);
        referencesRepository.saveAndFlush(references);
        Long itemsId = items.getId();

        // Get all the referencesList where items equals to itemsId
        defaultReferencesShouldBeFound("itemsId.equals=" + itemsId);

        // Get all the referencesList where items equals to itemsId + 1
        defaultReferencesShouldNotBeFound("itemsId.equals=" + (itemsId + 1));
    }


    @Test
    @Transactional
    public void getAllReferencesByProjecsIsEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);
        Projecs projecs = ProjecsResourceIT.createEntity(em);
        em.persist(projecs);
        em.flush();
        references.addProjecs(projecs);
        referencesRepository.saveAndFlush(references);
        Long projecsId = projecs.getId();

        // Get all the referencesList where projecs equals to projecsId
        defaultReferencesShouldBeFound("projecsId.equals=" + projecsId);

        // Get all the referencesList where projecs equals to projecsId + 1
        defaultReferencesShouldNotBeFound("projecsId.equals=" + (projecsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultReferencesShouldBeFound(String filter) throws Exception {
        restReferencesMockMvc.perform(get("/api/references?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(references.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)));

        // Check, that the count call also returns 1
        restReferencesMockMvc.perform(get("/api/references/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultReferencesShouldNotBeFound(String filter) throws Exception {
        restReferencesMockMvc.perform(get("/api/references?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restReferencesMockMvc.perform(get("/api/references/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingReferences() throws Exception {
        // Get the references
        restReferencesMockMvc.perform(get("/api/references/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();

        // Update the references
        References updatedReferences = referencesRepository.findById(references.getId()).get();
        // Disconnect from session so that the updates on updatedReferences are not directly saved in db
        em.detach(updatedReferences);
        updatedReferences
            .name(UPDATED_NAME)
            .notes(UPDATED_NOTES);
        ReferencesDTO referencesDTO = referencesMapper.toDto(updatedReferences);

        restReferencesMockMvc.perform(put("/api/references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isOk());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
        References testReferences = referencesList.get(referencesList.size() - 1);
        assertThat(testReferences.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testReferences.getNotes()).isEqualTo(UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void updateNonExistingReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReferencesMockMvc.perform(put("/api/references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        int databaseSizeBeforeDelete = referencesRepository.findAll().size();

        // Delete the references
        restReferencesMockMvc.perform(delete("/api/references/{id}", references.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(References.class);
        References references1 = new References();
        references1.setId(1L);
        References references2 = new References();
        references2.setId(references1.getId());
        assertThat(references1).isEqualTo(references2);
        references2.setId(2L);
        assertThat(references1).isNotEqualTo(references2);
        references1.setId(null);
        assertThat(references1).isNotEqualTo(references2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReferencesDTO.class);
        ReferencesDTO referencesDTO1 = new ReferencesDTO();
        referencesDTO1.setId(1L);
        ReferencesDTO referencesDTO2 = new ReferencesDTO();
        assertThat(referencesDTO1).isNotEqualTo(referencesDTO2);
        referencesDTO2.setId(referencesDTO1.getId());
        assertThat(referencesDTO1).isEqualTo(referencesDTO2);
        referencesDTO2.setId(2L);
        assertThat(referencesDTO1).isNotEqualTo(referencesDTO2);
        referencesDTO1.setId(null);
        assertThat(referencesDTO1).isNotEqualTo(referencesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(referencesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(referencesMapper.fromId(null)).isNull();
    }
}
