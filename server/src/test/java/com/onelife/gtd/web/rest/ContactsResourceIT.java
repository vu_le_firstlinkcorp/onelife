package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.Contacts;
import com.onelife.gtd.domain.Items;
import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.repository.ContactsRepository;
import com.onelife.gtd.service.ContactsService;
import com.onelife.gtd.service.dto.ContactsDTO;
import com.onelife.gtd.service.mapper.ContactsMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.ContactsCriteria;
import com.onelife.gtd.service.ContactsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactsResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class ContactsResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ContactsRepository contactsRepository;

    @Autowired
    private ContactsMapper contactsMapper;

    @Autowired
    private ContactsService contactsService;

    @Autowired
    private ContactsQueryService contactsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restContactsMockMvc;

    private Contacts contacts;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContactsResource contactsResource = new ContactsResource(contactsService, contactsQueryService);
        this.restContactsMockMvc = MockMvcBuilders.standaloneSetup(contactsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contacts createEntity(EntityManager em) {
        Contacts contacts = new Contacts()
            .name(DEFAULT_NAME);
        return contacts;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contacts createUpdatedEntity(EntityManager em) {
        Contacts contacts = new Contacts()
            .name(UPDATED_NAME);
        return contacts;
    }

    @BeforeEach
    public void initTest() {
        contacts = createEntity(em);
    }

    @Test
    @Transactional
    public void createContacts() throws Exception {
        int databaseSizeBeforeCreate = contactsRepository.findAll().size();

        // Create the Contacts
        ContactsDTO contactsDTO = contactsMapper.toDto(contacts);
        restContactsMockMvc.perform(post("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactsDTO)))
            .andExpect(status().isCreated());

        // Validate the Contacts in the database
        List<Contacts> contactsList = contactsRepository.findAll();
        assertThat(contactsList).hasSize(databaseSizeBeforeCreate + 1);
        Contacts testContacts = contactsList.get(contactsList.size() - 1);
        assertThat(testContacts.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createContactsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactsRepository.findAll().size();

        // Create the Contacts with an existing ID
        contacts.setId(1L);
        ContactsDTO contactsDTO = contactsMapper.toDto(contacts);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactsMockMvc.perform(post("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Contacts in the database
        List<Contacts> contactsList = contactsRepository.findAll();
        assertThat(contactsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllContacts() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList
        restContactsMockMvc.perform(get("/api/contacts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contacts.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getContacts() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get the contacts
        restContactsMockMvc.perform(get("/api/contacts/{id}", contacts.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contacts.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getAllContactsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList where name equals to DEFAULT_NAME
        defaultContactsShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the contactsList where name equals to UPDATED_NAME
        defaultContactsShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList where name not equals to DEFAULT_NAME
        defaultContactsShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the contactsList where name not equals to UPDATED_NAME
        defaultContactsShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList where name in DEFAULT_NAME or UPDATED_NAME
        defaultContactsShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the contactsList where name equals to UPDATED_NAME
        defaultContactsShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList where name is not null
        defaultContactsShouldBeFound("name.specified=true");

        // Get all the contactsList where name is null
        defaultContactsShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByNameContainsSomething() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList where name contains DEFAULT_NAME
        defaultContactsShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the contactsList where name contains UPDATED_NAME
        defaultContactsShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        // Get all the contactsList where name does not contain DEFAULT_NAME
        defaultContactsShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the contactsList where name does not contain UPDATED_NAME
        defaultContactsShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllContactsByItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);
        Items items = ItemsResourceIT.createEntity(em);
        em.persist(items);
        em.flush();
        contacts.setItems(items);
        contactsRepository.saveAndFlush(contacts);
        Long itemsId = items.getId();

        // Get all the contactsList where items equals to itemsId
        defaultContactsShouldBeFound("itemsId.equals=" + itemsId);

        // Get all the contactsList where items equals to itemsId + 1
        defaultContactsShouldNotBeFound("itemsId.equals=" + (itemsId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByProjecsIsEqualToSomething() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);
        Projecs projecs = ProjecsResourceIT.createEntity(em);
        em.persist(projecs);
        em.flush();
        contacts.setProjecs(projecs);
        contactsRepository.saveAndFlush(contacts);
        Long projecsId = projecs.getId();

        // Get all the contactsList where projecs equals to projecsId
        defaultContactsShouldBeFound("projecsId.equals=" + projecsId);

        // Get all the contactsList where projecs equals to projecsId + 1
        defaultContactsShouldNotBeFound("projecsId.equals=" + (projecsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactsShouldBeFound(String filter) throws Exception {
        restContactsMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contacts.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restContactsMockMvc.perform(get("/api/contacts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactsShouldNotBeFound(String filter) throws Exception {
        restContactsMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactsMockMvc.perform(get("/api/contacts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingContacts() throws Exception {
        // Get the contacts
        restContactsMockMvc.perform(get("/api/contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContacts() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        int databaseSizeBeforeUpdate = contactsRepository.findAll().size();

        // Update the contacts
        Contacts updatedContacts = contactsRepository.findById(contacts.getId()).get();
        // Disconnect from session so that the updates on updatedContacts are not directly saved in db
        em.detach(updatedContacts);
        updatedContacts
            .name(UPDATED_NAME);
        ContactsDTO contactsDTO = contactsMapper.toDto(updatedContacts);

        restContactsMockMvc.perform(put("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactsDTO)))
            .andExpect(status().isOk());

        // Validate the Contacts in the database
        List<Contacts> contactsList = contactsRepository.findAll();
        assertThat(contactsList).hasSize(databaseSizeBeforeUpdate);
        Contacts testContacts = contactsList.get(contactsList.size() - 1);
        assertThat(testContacts.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingContacts() throws Exception {
        int databaseSizeBeforeUpdate = contactsRepository.findAll().size();

        // Create the Contacts
        ContactsDTO contactsDTO = contactsMapper.toDto(contacts);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactsMockMvc.perform(put("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Contacts in the database
        List<Contacts> contactsList = contactsRepository.findAll();
        assertThat(contactsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContacts() throws Exception {
        // Initialize the database
        contactsRepository.saveAndFlush(contacts);

        int databaseSizeBeforeDelete = contactsRepository.findAll().size();

        // Delete the contacts
        restContactsMockMvc.perform(delete("/api/contacts/{id}", contacts.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Contacts> contactsList = contactsRepository.findAll();
        assertThat(contactsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Contacts.class);
        Contacts contacts1 = new Contacts();
        contacts1.setId(1L);
        Contacts contacts2 = new Contacts();
        contacts2.setId(contacts1.getId());
        assertThat(contacts1).isEqualTo(contacts2);
        contacts2.setId(2L);
        assertThat(contacts1).isNotEqualTo(contacts2);
        contacts1.setId(null);
        assertThat(contacts1).isNotEqualTo(contacts2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactsDTO.class);
        ContactsDTO contactsDTO1 = new ContactsDTO();
        contactsDTO1.setId(1L);
        ContactsDTO contactsDTO2 = new ContactsDTO();
        assertThat(contactsDTO1).isNotEqualTo(contactsDTO2);
        contactsDTO2.setId(contactsDTO1.getId());
        assertThat(contactsDTO1).isEqualTo(contactsDTO2);
        contactsDTO2.setId(2L);
        assertThat(contactsDTO1).isNotEqualTo(contactsDTO2);
        contactsDTO1.setId(null);
        assertThat(contactsDTO1).isNotEqualTo(contactsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(contactsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(contactsMapper.fromId(null)).isNull();
    }
}
