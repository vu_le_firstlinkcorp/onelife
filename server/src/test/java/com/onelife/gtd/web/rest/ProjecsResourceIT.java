package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.domain.Items;
import com.onelife.gtd.domain.Contacts;
import com.onelife.gtd.domain.Areas;
import com.onelife.gtd.domain.Tags;
import com.onelife.gtd.domain.References;
import com.onelife.gtd.repository.ProjecsRepository;
import com.onelife.gtd.service.ProjecsService;
import com.onelife.gtd.service.dto.ProjecsDTO;
import com.onelife.gtd.service.mapper.ProjecsMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.ProjecsCriteria;
import com.onelife.gtd.service.ProjecsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProjecsResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class ProjecsResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DUE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DUE_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_PROJECT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PROJECT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_FOCUS = "AAAAAAAAAA";
    private static final String UPDATED_FOCUS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    @Autowired
    private ProjecsRepository projecsRepository;

    @Mock
    private ProjecsRepository projecsRepositoryMock;

    @Autowired
    private ProjecsMapper projecsMapper;

    @Mock
    private ProjecsService projecsServiceMock;

    @Autowired
    private ProjecsService projecsService;

    @Autowired
    private ProjecsQueryService projecsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProjecsMockMvc;

    private Projecs projecs;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProjecsResource projecsResource = new ProjecsResource(projecsService, projecsQueryService);
        this.restProjecsMockMvc = MockMvcBuilders.standaloneSetup(projecsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Projecs createEntity(EntityManager em) {
        Projecs projecs = new Projecs()
            .name(DEFAULT_NAME)
            .notes(DEFAULT_NOTES)
            .dueDate(DEFAULT_DUE_DATE)
            .projectType(DEFAULT_PROJECT_TYPE)
            .focus(DEFAULT_FOCUS)
            .status(DEFAULT_STATUS);
        return projecs;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Projecs createUpdatedEntity(EntityManager em) {
        Projecs projecs = new Projecs()
            .name(UPDATED_NAME)
            .notes(UPDATED_NOTES)
            .dueDate(UPDATED_DUE_DATE)
            .projectType(UPDATED_PROJECT_TYPE)
            .focus(UPDATED_FOCUS)
            .status(UPDATED_STATUS);
        return projecs;
    }

    @BeforeEach
    public void initTest() {
        projecs = createEntity(em);
    }

    @Test
    @Transactional
    public void createProjecs() throws Exception {
        int databaseSizeBeforeCreate = projecsRepository.findAll().size();

        // Create the Projecs
        ProjecsDTO projecsDTO = projecsMapper.toDto(projecs);
        restProjecsMockMvc.perform(post("/api/projecs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projecsDTO)))
            .andExpect(status().isCreated());

        // Validate the Projecs in the database
        List<Projecs> projecsList = projecsRepository.findAll();
        assertThat(projecsList).hasSize(databaseSizeBeforeCreate + 1);
        Projecs testProjecs = projecsList.get(projecsList.size() - 1);
        assertThat(testProjecs.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProjecs.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testProjecs.getDueDate()).isEqualTo(DEFAULT_DUE_DATE);
        assertThat(testProjecs.getProjectType()).isEqualTo(DEFAULT_PROJECT_TYPE);
        assertThat(testProjecs.getFocus()).isEqualTo(DEFAULT_FOCUS);
        assertThat(testProjecs.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createProjecsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projecsRepository.findAll().size();

        // Create the Projecs with an existing ID
        projecs.setId(1L);
        ProjecsDTO projecsDTO = projecsMapper.toDto(projecs);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjecsMockMvc.perform(post("/api/projecs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projecsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Projecs in the database
        List<Projecs> projecsList = projecsRepository.findAll();
        assertThat(projecsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProjecs() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList
        restProjecsMockMvc.perform(get("/api/projecs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projecs.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].projectType").value(hasItem(DEFAULT_PROJECT_TYPE)))
            .andExpect(jsonPath("$.[*].focus").value(hasItem(DEFAULT_FOCUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllProjecsWithEagerRelationshipsIsEnabled() throws Exception {
        ProjecsResource projecsResource = new ProjecsResource(projecsServiceMock, projecsQueryService);
        when(projecsServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restProjecsMockMvc = MockMvcBuilders.standaloneSetup(projecsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restProjecsMockMvc.perform(get("/api/projecs?eagerload=true"))
        .andExpect(status().isOk());

        verify(projecsServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllProjecsWithEagerRelationshipsIsNotEnabled() throws Exception {
        ProjecsResource projecsResource = new ProjecsResource(projecsServiceMock, projecsQueryService);
            when(projecsServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restProjecsMockMvc = MockMvcBuilders.standaloneSetup(projecsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restProjecsMockMvc.perform(get("/api/projecs?eagerload=true"))
        .andExpect(status().isOk());

            verify(projecsServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getProjecs() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get the projecs
        restProjecsMockMvc.perform(get("/api/projecs/{id}", projecs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(projecs.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES))
            .andExpect(jsonPath("$.dueDate").value(DEFAULT_DUE_DATE.toString()))
            .andExpect(jsonPath("$.projectType").value(DEFAULT_PROJECT_TYPE))
            .andExpect(jsonPath("$.focus").value(DEFAULT_FOCUS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getAllProjecsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where name equals to DEFAULT_NAME
        defaultProjecsShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the projecsList where name equals to UPDATED_NAME
        defaultProjecsShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProjecsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where name not equals to DEFAULT_NAME
        defaultProjecsShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the projecsList where name not equals to UPDATED_NAME
        defaultProjecsShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProjecsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProjecsShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the projecsList where name equals to UPDATED_NAME
        defaultProjecsShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProjecsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where name is not null
        defaultProjecsShouldBeFound("name.specified=true");

        // Get all the projecsList where name is null
        defaultProjecsShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllProjecsByNameContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where name contains DEFAULT_NAME
        defaultProjecsShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the projecsList where name contains UPDATED_NAME
        defaultProjecsShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProjecsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where name does not contain DEFAULT_NAME
        defaultProjecsShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the projecsList where name does not contain UPDATED_NAME
        defaultProjecsShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllProjecsByNotesIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where notes equals to DEFAULT_NOTES
        defaultProjecsShouldBeFound("notes.equals=" + DEFAULT_NOTES);

        // Get all the projecsList where notes equals to UPDATED_NOTES
        defaultProjecsShouldNotBeFound("notes.equals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllProjecsByNotesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where notes not equals to DEFAULT_NOTES
        defaultProjecsShouldNotBeFound("notes.notEquals=" + DEFAULT_NOTES);

        // Get all the projecsList where notes not equals to UPDATED_NOTES
        defaultProjecsShouldBeFound("notes.notEquals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllProjecsByNotesIsInShouldWork() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where notes in DEFAULT_NOTES or UPDATED_NOTES
        defaultProjecsShouldBeFound("notes.in=" + DEFAULT_NOTES + "," + UPDATED_NOTES);

        // Get all the projecsList where notes equals to UPDATED_NOTES
        defaultProjecsShouldNotBeFound("notes.in=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllProjecsByNotesIsNullOrNotNull() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where notes is not null
        defaultProjecsShouldBeFound("notes.specified=true");

        // Get all the projecsList where notes is null
        defaultProjecsShouldNotBeFound("notes.specified=false");
    }
                @Test
    @Transactional
    public void getAllProjecsByNotesContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where notes contains DEFAULT_NOTES
        defaultProjecsShouldBeFound("notes.contains=" + DEFAULT_NOTES);

        // Get all the projecsList where notes contains UPDATED_NOTES
        defaultProjecsShouldNotBeFound("notes.contains=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllProjecsByNotesNotContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where notes does not contain DEFAULT_NOTES
        defaultProjecsShouldNotBeFound("notes.doesNotContain=" + DEFAULT_NOTES);

        // Get all the projecsList where notes does not contain UPDATED_NOTES
        defaultProjecsShouldBeFound("notes.doesNotContain=" + UPDATED_NOTES);
    }


    @Test
    @Transactional
    public void getAllProjecsByDueDateIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate equals to DEFAULT_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.equals=" + DEFAULT_DUE_DATE);

        // Get all the projecsList where dueDate equals to UPDATED_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.equals=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate not equals to DEFAULT_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.notEquals=" + DEFAULT_DUE_DATE);

        // Get all the projecsList where dueDate not equals to UPDATED_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.notEquals=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsInShouldWork() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate in DEFAULT_DUE_DATE or UPDATED_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.in=" + DEFAULT_DUE_DATE + "," + UPDATED_DUE_DATE);

        // Get all the projecsList where dueDate equals to UPDATED_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.in=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate is not null
        defaultProjecsShouldBeFound("dueDate.specified=true");

        // Get all the projecsList where dueDate is null
        defaultProjecsShouldNotBeFound("dueDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate is greater than or equal to DEFAULT_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.greaterThanOrEqual=" + DEFAULT_DUE_DATE);

        // Get all the projecsList where dueDate is greater than or equal to UPDATED_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.greaterThanOrEqual=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate is less than or equal to DEFAULT_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.lessThanOrEqual=" + DEFAULT_DUE_DATE);

        // Get all the projecsList where dueDate is less than or equal to SMALLER_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.lessThanOrEqual=" + SMALLER_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsLessThanSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate is less than DEFAULT_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.lessThan=" + DEFAULT_DUE_DATE);

        // Get all the projecsList where dueDate is less than UPDATED_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.lessThan=" + UPDATED_DUE_DATE);
    }

    @Test
    @Transactional
    public void getAllProjecsByDueDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where dueDate is greater than DEFAULT_DUE_DATE
        defaultProjecsShouldNotBeFound("dueDate.greaterThan=" + DEFAULT_DUE_DATE);

        // Get all the projecsList where dueDate is greater than SMALLER_DUE_DATE
        defaultProjecsShouldBeFound("dueDate.greaterThan=" + SMALLER_DUE_DATE);
    }


    @Test
    @Transactional
    public void getAllProjecsByProjectTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where projectType equals to DEFAULT_PROJECT_TYPE
        defaultProjecsShouldBeFound("projectType.equals=" + DEFAULT_PROJECT_TYPE);

        // Get all the projecsList where projectType equals to UPDATED_PROJECT_TYPE
        defaultProjecsShouldNotBeFound("projectType.equals=" + UPDATED_PROJECT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProjecsByProjectTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where projectType not equals to DEFAULT_PROJECT_TYPE
        defaultProjecsShouldNotBeFound("projectType.notEquals=" + DEFAULT_PROJECT_TYPE);

        // Get all the projecsList where projectType not equals to UPDATED_PROJECT_TYPE
        defaultProjecsShouldBeFound("projectType.notEquals=" + UPDATED_PROJECT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProjecsByProjectTypeIsInShouldWork() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where projectType in DEFAULT_PROJECT_TYPE or UPDATED_PROJECT_TYPE
        defaultProjecsShouldBeFound("projectType.in=" + DEFAULT_PROJECT_TYPE + "," + UPDATED_PROJECT_TYPE);

        // Get all the projecsList where projectType equals to UPDATED_PROJECT_TYPE
        defaultProjecsShouldNotBeFound("projectType.in=" + UPDATED_PROJECT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProjecsByProjectTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where projectType is not null
        defaultProjecsShouldBeFound("projectType.specified=true");

        // Get all the projecsList where projectType is null
        defaultProjecsShouldNotBeFound("projectType.specified=false");
    }
                @Test
    @Transactional
    public void getAllProjecsByProjectTypeContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where projectType contains DEFAULT_PROJECT_TYPE
        defaultProjecsShouldBeFound("projectType.contains=" + DEFAULT_PROJECT_TYPE);

        // Get all the projecsList where projectType contains UPDATED_PROJECT_TYPE
        defaultProjecsShouldNotBeFound("projectType.contains=" + UPDATED_PROJECT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProjecsByProjectTypeNotContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where projectType does not contain DEFAULT_PROJECT_TYPE
        defaultProjecsShouldNotBeFound("projectType.doesNotContain=" + DEFAULT_PROJECT_TYPE);

        // Get all the projecsList where projectType does not contain UPDATED_PROJECT_TYPE
        defaultProjecsShouldBeFound("projectType.doesNotContain=" + UPDATED_PROJECT_TYPE);
    }


    @Test
    @Transactional
    public void getAllProjecsByFocusIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where focus equals to DEFAULT_FOCUS
        defaultProjecsShouldBeFound("focus.equals=" + DEFAULT_FOCUS);

        // Get all the projecsList where focus equals to UPDATED_FOCUS
        defaultProjecsShouldNotBeFound("focus.equals=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByFocusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where focus not equals to DEFAULT_FOCUS
        defaultProjecsShouldNotBeFound("focus.notEquals=" + DEFAULT_FOCUS);

        // Get all the projecsList where focus not equals to UPDATED_FOCUS
        defaultProjecsShouldBeFound("focus.notEquals=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByFocusIsInShouldWork() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where focus in DEFAULT_FOCUS or UPDATED_FOCUS
        defaultProjecsShouldBeFound("focus.in=" + DEFAULT_FOCUS + "," + UPDATED_FOCUS);

        // Get all the projecsList where focus equals to UPDATED_FOCUS
        defaultProjecsShouldNotBeFound("focus.in=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByFocusIsNullOrNotNull() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where focus is not null
        defaultProjecsShouldBeFound("focus.specified=true");

        // Get all the projecsList where focus is null
        defaultProjecsShouldNotBeFound("focus.specified=false");
    }
                @Test
    @Transactional
    public void getAllProjecsByFocusContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where focus contains DEFAULT_FOCUS
        defaultProjecsShouldBeFound("focus.contains=" + DEFAULT_FOCUS);

        // Get all the projecsList where focus contains UPDATED_FOCUS
        defaultProjecsShouldNotBeFound("focus.contains=" + UPDATED_FOCUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByFocusNotContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where focus does not contain DEFAULT_FOCUS
        defaultProjecsShouldNotBeFound("focus.doesNotContain=" + DEFAULT_FOCUS);

        // Get all the projecsList where focus does not contain UPDATED_FOCUS
        defaultProjecsShouldBeFound("focus.doesNotContain=" + UPDATED_FOCUS);
    }


    @Test
    @Transactional
    public void getAllProjecsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where status equals to DEFAULT_STATUS
        defaultProjecsShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the projecsList where status equals to UPDATED_STATUS
        defaultProjecsShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where status not equals to DEFAULT_STATUS
        defaultProjecsShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the projecsList where status not equals to UPDATED_STATUS
        defaultProjecsShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultProjecsShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the projecsList where status equals to UPDATED_STATUS
        defaultProjecsShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where status is not null
        defaultProjecsShouldBeFound("status.specified=true");

        // Get all the projecsList where status is null
        defaultProjecsShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllProjecsByStatusContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where status contains DEFAULT_STATUS
        defaultProjecsShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the projecsList where status contains UPDATED_STATUS
        defaultProjecsShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProjecsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        // Get all the projecsList where status does not contain DEFAULT_STATUS
        defaultProjecsShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the projecsList where status does not contain UPDATED_STATUS
        defaultProjecsShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllProjecsByItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);
        Items items = ItemsResourceIT.createEntity(em);
        em.persist(items);
        em.flush();
        projecs.setItems(items);
        projecsRepository.saveAndFlush(projecs);
        Long itemsId = items.getId();

        // Get all the projecsList where items equals to itemsId
        defaultProjecsShouldBeFound("itemsId.equals=" + itemsId);

        // Get all the projecsList where items equals to itemsId + 1
        defaultProjecsShouldNotBeFound("itemsId.equals=" + (itemsId + 1));
    }


    @Test
    @Transactional
    public void getAllProjecsByContactsIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);
        Contacts contacts = ContactsResourceIT.createEntity(em);
        em.persist(contacts);
        em.flush();
        projecs.addContacts(contacts);
        projecsRepository.saveAndFlush(projecs);
        Long contactsId = contacts.getId();

        // Get all the projecsList where contacts equals to contactsId
        defaultProjecsShouldBeFound("contactsId.equals=" + contactsId);

        // Get all the projecsList where contacts equals to contactsId + 1
        defaultProjecsShouldNotBeFound("contactsId.equals=" + (contactsId + 1));
    }


    @Test
    @Transactional
    public void getAllProjecsByAreasIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);
        Areas areas = AreasResourceIT.createEntity(em);
        em.persist(areas);
        em.flush();
        projecs.addAreas(areas);
        projecsRepository.saveAndFlush(projecs);
        Long areasId = areas.getId();

        // Get all the projecsList where areas equals to areasId
        defaultProjecsShouldBeFound("areasId.equals=" + areasId);

        // Get all the projecsList where areas equals to areasId + 1
        defaultProjecsShouldNotBeFound("areasId.equals=" + (areasId + 1));
    }


    @Test
    @Transactional
    public void getAllProjecsByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);
        Tags tags = TagsResourceIT.createEntity(em);
        em.persist(tags);
        em.flush();
        projecs.addTags(tags);
        projecsRepository.saveAndFlush(projecs);
        Long tagsId = tags.getId();

        // Get all the projecsList where tags equals to tagsId
        defaultProjecsShouldBeFound("tagsId.equals=" + tagsId);

        // Get all the projecsList where tags equals to tagsId + 1
        defaultProjecsShouldNotBeFound("tagsId.equals=" + (tagsId + 1));
    }


    @Test
    @Transactional
    public void getAllProjecsByReferencesIsEqualToSomething() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);
        References references = ReferencesResourceIT.createEntity(em);
        em.persist(references);
        em.flush();
        projecs.addReferences(references);
        projecsRepository.saveAndFlush(projecs);
        Long referencesId = references.getId();

        // Get all the projecsList where references equals to referencesId
        defaultProjecsShouldBeFound("referencesId.equals=" + referencesId);

        // Get all the projecsList where references equals to referencesId + 1
        defaultProjecsShouldNotBeFound("referencesId.equals=" + (referencesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProjecsShouldBeFound(String filter) throws Exception {
        restProjecsMockMvc.perform(get("/api/projecs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projecs.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].projectType").value(hasItem(DEFAULT_PROJECT_TYPE)))
            .andExpect(jsonPath("$.[*].focus").value(hasItem(DEFAULT_FOCUS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));

        // Check, that the count call also returns 1
        restProjecsMockMvc.perform(get("/api/projecs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProjecsShouldNotBeFound(String filter) throws Exception {
        restProjecsMockMvc.perform(get("/api/projecs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProjecsMockMvc.perform(get("/api/projecs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProjecs() throws Exception {
        // Get the projecs
        restProjecsMockMvc.perform(get("/api/projecs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProjecs() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        int databaseSizeBeforeUpdate = projecsRepository.findAll().size();

        // Update the projecs
        Projecs updatedProjecs = projecsRepository.findById(projecs.getId()).get();
        // Disconnect from session so that the updates on updatedProjecs are not directly saved in db
        em.detach(updatedProjecs);
        updatedProjecs
            .name(UPDATED_NAME)
            .notes(UPDATED_NOTES)
            .dueDate(UPDATED_DUE_DATE)
            .projectType(UPDATED_PROJECT_TYPE)
            .focus(UPDATED_FOCUS)
            .status(UPDATED_STATUS);
        ProjecsDTO projecsDTO = projecsMapper.toDto(updatedProjecs);

        restProjecsMockMvc.perform(put("/api/projecs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projecsDTO)))
            .andExpect(status().isOk());

        // Validate the Projecs in the database
        List<Projecs> projecsList = projecsRepository.findAll();
        assertThat(projecsList).hasSize(databaseSizeBeforeUpdate);
        Projecs testProjecs = projecsList.get(projecsList.size() - 1);
        assertThat(testProjecs.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProjecs.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testProjecs.getDueDate()).isEqualTo(UPDATED_DUE_DATE);
        assertThat(testProjecs.getProjectType()).isEqualTo(UPDATED_PROJECT_TYPE);
        assertThat(testProjecs.getFocus()).isEqualTo(UPDATED_FOCUS);
        assertThat(testProjecs.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingProjecs() throws Exception {
        int databaseSizeBeforeUpdate = projecsRepository.findAll().size();

        // Create the Projecs
        ProjecsDTO projecsDTO = projecsMapper.toDto(projecs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjecsMockMvc.perform(put("/api/projecs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projecsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Projecs in the database
        List<Projecs> projecsList = projecsRepository.findAll();
        assertThat(projecsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProjecs() throws Exception {
        // Initialize the database
        projecsRepository.saveAndFlush(projecs);

        int databaseSizeBeforeDelete = projecsRepository.findAll().size();

        // Delete the projecs
        restProjecsMockMvc.perform(delete("/api/projecs/{id}", projecs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Projecs> projecsList = projecsRepository.findAll();
        assertThat(projecsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Projecs.class);
        Projecs projecs1 = new Projecs();
        projecs1.setId(1L);
        Projecs projecs2 = new Projecs();
        projecs2.setId(projecs1.getId());
        assertThat(projecs1).isEqualTo(projecs2);
        projecs2.setId(2L);
        assertThat(projecs1).isNotEqualTo(projecs2);
        projecs1.setId(null);
        assertThat(projecs1).isNotEqualTo(projecs2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProjecsDTO.class);
        ProjecsDTO projecsDTO1 = new ProjecsDTO();
        projecsDTO1.setId(1L);
        ProjecsDTO projecsDTO2 = new ProjecsDTO();
        assertThat(projecsDTO1).isNotEqualTo(projecsDTO2);
        projecsDTO2.setId(projecsDTO1.getId());
        assertThat(projecsDTO1).isEqualTo(projecsDTO2);
        projecsDTO2.setId(2L);
        assertThat(projecsDTO1).isNotEqualTo(projecsDTO2);
        projecsDTO1.setId(null);
        assertThat(projecsDTO1).isNotEqualTo(projecsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(projecsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(projecsMapper.fromId(null)).isNull();
    }
}
