package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.Areas;
import com.onelife.gtd.domain.Items;
import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.repository.AreasRepository;
import com.onelife.gtd.service.AreasService;
import com.onelife.gtd.service.dto.AreasDTO;
import com.onelife.gtd.service.mapper.AreasMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.AreasCriteria;
import com.onelife.gtd.service.AreasQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AreasResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class AreasResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private AreasRepository areasRepository;

    @Autowired
    private AreasMapper areasMapper;

    @Autowired
    private AreasService areasService;

    @Autowired
    private AreasQueryService areasQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAreasMockMvc;

    private Areas areas;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AreasResource areasResource = new AreasResource(areasService, areasQueryService);
        this.restAreasMockMvc = MockMvcBuilders.standaloneSetup(areasResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Areas createEntity(EntityManager em) {
        Areas areas = new Areas()
            .name(DEFAULT_NAME);
        return areas;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Areas createUpdatedEntity(EntityManager em) {
        Areas areas = new Areas()
            .name(UPDATED_NAME);
        return areas;
    }

    @BeforeEach
    public void initTest() {
        areas = createEntity(em);
    }

    @Test
    @Transactional
    public void createAreas() throws Exception {
        int databaseSizeBeforeCreate = areasRepository.findAll().size();

        // Create the Areas
        AreasDTO areasDTO = areasMapper.toDto(areas);
        restAreasMockMvc.perform(post("/api/areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areasDTO)))
            .andExpect(status().isCreated());

        // Validate the Areas in the database
        List<Areas> areasList = areasRepository.findAll();
        assertThat(areasList).hasSize(databaseSizeBeforeCreate + 1);
        Areas testAreas = areasList.get(areasList.size() - 1);
        assertThat(testAreas.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createAreasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = areasRepository.findAll().size();

        // Create the Areas with an existing ID
        areas.setId(1L);
        AreasDTO areasDTO = areasMapper.toDto(areas);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAreasMockMvc.perform(post("/api/areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Areas in the database
        List<Areas> areasList = areasRepository.findAll();
        assertThat(areasList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAreas() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList
        restAreasMockMvc.perform(get("/api/areas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(areas.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getAreas() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get the areas
        restAreasMockMvc.perform(get("/api/areas/{id}", areas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(areas.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getAllAreasByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList where name equals to DEFAULT_NAME
        defaultAreasShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the areasList where name equals to UPDATED_NAME
        defaultAreasShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAreasByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList where name not equals to DEFAULT_NAME
        defaultAreasShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the areasList where name not equals to UPDATED_NAME
        defaultAreasShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAreasByNameIsInShouldWork() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList where name in DEFAULT_NAME or UPDATED_NAME
        defaultAreasShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the areasList where name equals to UPDATED_NAME
        defaultAreasShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAreasByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList where name is not null
        defaultAreasShouldBeFound("name.specified=true");

        // Get all the areasList where name is null
        defaultAreasShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllAreasByNameContainsSomething() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList where name contains DEFAULT_NAME
        defaultAreasShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the areasList where name contains UPDATED_NAME
        defaultAreasShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAreasByNameNotContainsSomething() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        // Get all the areasList where name does not contain DEFAULT_NAME
        defaultAreasShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the areasList where name does not contain UPDATED_NAME
        defaultAreasShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllAreasByItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);
        Items items = ItemsResourceIT.createEntity(em);
        em.persist(items);
        em.flush();
        areas.setItems(items);
        areasRepository.saveAndFlush(areas);
        Long itemsId = items.getId();

        // Get all the areasList where items equals to itemsId
        defaultAreasShouldBeFound("itemsId.equals=" + itemsId);

        // Get all the areasList where items equals to itemsId + 1
        defaultAreasShouldNotBeFound("itemsId.equals=" + (itemsId + 1));
    }


    @Test
    @Transactional
    public void getAllAreasByProjecsIsEqualToSomething() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);
        Projecs projecs = ProjecsResourceIT.createEntity(em);
        em.persist(projecs);
        em.flush();
        areas.setProjecs(projecs);
        areasRepository.saveAndFlush(areas);
        Long projecsId = projecs.getId();

        // Get all the areasList where projecs equals to projecsId
        defaultAreasShouldBeFound("projecsId.equals=" + projecsId);

        // Get all the areasList where projecs equals to projecsId + 1
        defaultAreasShouldNotBeFound("projecsId.equals=" + (projecsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAreasShouldBeFound(String filter) throws Exception {
        restAreasMockMvc.perform(get("/api/areas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(areas.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restAreasMockMvc.perform(get("/api/areas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAreasShouldNotBeFound(String filter) throws Exception {
        restAreasMockMvc.perform(get("/api/areas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAreasMockMvc.perform(get("/api/areas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAreas() throws Exception {
        // Get the areas
        restAreasMockMvc.perform(get("/api/areas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAreas() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        int databaseSizeBeforeUpdate = areasRepository.findAll().size();

        // Update the areas
        Areas updatedAreas = areasRepository.findById(areas.getId()).get();
        // Disconnect from session so that the updates on updatedAreas are not directly saved in db
        em.detach(updatedAreas);
        updatedAreas
            .name(UPDATED_NAME);
        AreasDTO areasDTO = areasMapper.toDto(updatedAreas);

        restAreasMockMvc.perform(put("/api/areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areasDTO)))
            .andExpect(status().isOk());

        // Validate the Areas in the database
        List<Areas> areasList = areasRepository.findAll();
        assertThat(areasList).hasSize(databaseSizeBeforeUpdate);
        Areas testAreas = areasList.get(areasList.size() - 1);
        assertThat(testAreas.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingAreas() throws Exception {
        int databaseSizeBeforeUpdate = areasRepository.findAll().size();

        // Create the Areas
        AreasDTO areasDTO = areasMapper.toDto(areas);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAreasMockMvc.perform(put("/api/areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Areas in the database
        List<Areas> areasList = areasRepository.findAll();
        assertThat(areasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAreas() throws Exception {
        // Initialize the database
        areasRepository.saveAndFlush(areas);

        int databaseSizeBeforeDelete = areasRepository.findAll().size();

        // Delete the areas
        restAreasMockMvc.perform(delete("/api/areas/{id}", areas.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Areas> areasList = areasRepository.findAll();
        assertThat(areasList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Areas.class);
        Areas areas1 = new Areas();
        areas1.setId(1L);
        Areas areas2 = new Areas();
        areas2.setId(areas1.getId());
        assertThat(areas1).isEqualTo(areas2);
        areas2.setId(2L);
        assertThat(areas1).isNotEqualTo(areas2);
        areas1.setId(null);
        assertThat(areas1).isNotEqualTo(areas2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AreasDTO.class);
        AreasDTO areasDTO1 = new AreasDTO();
        areasDTO1.setId(1L);
        AreasDTO areasDTO2 = new AreasDTO();
        assertThat(areasDTO1).isNotEqualTo(areasDTO2);
        areasDTO2.setId(areasDTO1.getId());
        assertThat(areasDTO1).isEqualTo(areasDTO2);
        areasDTO2.setId(2L);
        assertThat(areasDTO1).isNotEqualTo(areasDTO2);
        areasDTO1.setId(null);
        assertThat(areasDTO1).isNotEqualTo(areasDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(areasMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(areasMapper.fromId(null)).isNull();
    }
}
