package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.FeedBack;
import com.onelife.gtd.repository.FeedBackRepository;
import com.onelife.gtd.service.FeedBackService;
import com.onelife.gtd.service.dto.FeedBackDTO;
import com.onelife.gtd.service.mapper.FeedBackMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.FeedBackCriteria;
import com.onelife.gtd.service.FeedBackQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FeedBackResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class FeedBackResourceIT {

    private static final String DEFAULT_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_CONTEXT = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_FEED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FEED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_FEED_DATE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_FIX_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FIX_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_FIX_DATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private FeedBackRepository feedBackRepository;

    @Autowired
    private FeedBackMapper feedBackMapper;

    @Autowired
    private FeedBackService feedBackService;

    @Autowired
    private FeedBackQueryService feedBackQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFeedBackMockMvc;

    private FeedBack feedBack;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FeedBackResource feedBackResource = new FeedBackResource(feedBackService, feedBackQueryService);
        this.restFeedBackMockMvc = MockMvcBuilders.standaloneSetup(feedBackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeedBack createEntity(EntityManager em) {
        FeedBack feedBack = new FeedBack()
            .context(DEFAULT_CONTEXT)
            .status(DEFAULT_STATUS)
            .feedDate(DEFAULT_FEED_DATE)
            .fixDate(DEFAULT_FIX_DATE);
        return feedBack;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeedBack createUpdatedEntity(EntityManager em) {
        FeedBack feedBack = new FeedBack()
            .context(UPDATED_CONTEXT)
            .status(UPDATED_STATUS)
            .feedDate(UPDATED_FEED_DATE)
            .fixDate(UPDATED_FIX_DATE);
        return feedBack;
    }

    @BeforeEach
    public void initTest() {
        feedBack = createEntity(em);
    }

    @Test
    @Transactional
    public void createFeedBack() throws Exception {
        int databaseSizeBeforeCreate = feedBackRepository.findAll().size();

        // Create the FeedBack
        FeedBackDTO feedBackDTO = feedBackMapper.toDto(feedBack);
        restFeedBackMockMvc.perform(post("/api/feed-backs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedBackDTO)))
            .andExpect(status().isCreated());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeCreate + 1);
        FeedBack testFeedBack = feedBackList.get(feedBackList.size() - 1);
        assertThat(testFeedBack.getContext()).isEqualTo(DEFAULT_CONTEXT);
        assertThat(testFeedBack.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testFeedBack.getFeedDate()).isEqualTo(DEFAULT_FEED_DATE);
        assertThat(testFeedBack.getFixDate()).isEqualTo(DEFAULT_FIX_DATE);
    }

    @Test
    @Transactional
    public void createFeedBackWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feedBackRepository.findAll().size();

        // Create the FeedBack with an existing ID
        feedBack.setId(1L);
        FeedBackDTO feedBackDTO = feedBackMapper.toDto(feedBack);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeedBackMockMvc.perform(post("/api/feed-backs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedBackDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFeedBacks() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList
        restFeedBackMockMvc.perform(get("/api/feed-backs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feedBack.getId().intValue())))
            .andExpect(jsonPath("$.[*].context").value(hasItem(DEFAULT_CONTEXT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].feedDate").value(hasItem(DEFAULT_FEED_DATE.toString())))
            .andExpect(jsonPath("$.[*].fixDate").value(hasItem(DEFAULT_FIX_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getFeedBack() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get the feedBack
        restFeedBackMockMvc.perform(get("/api/feed-backs/{id}", feedBack.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(feedBack.getId().intValue()))
            .andExpect(jsonPath("$.context").value(DEFAULT_CONTEXT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.feedDate").value(DEFAULT_FEED_DATE.toString()))
            .andExpect(jsonPath("$.fixDate").value(DEFAULT_FIX_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllFeedBacksByContextIsEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where context equals to DEFAULT_CONTEXT
        defaultFeedBackShouldBeFound("context.equals=" + DEFAULT_CONTEXT);

        // Get all the feedBackList where context equals to UPDATED_CONTEXT
        defaultFeedBackShouldNotBeFound("context.equals=" + UPDATED_CONTEXT);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByContextIsNotEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where context not equals to DEFAULT_CONTEXT
        defaultFeedBackShouldNotBeFound("context.notEquals=" + DEFAULT_CONTEXT);

        // Get all the feedBackList where context not equals to UPDATED_CONTEXT
        defaultFeedBackShouldBeFound("context.notEquals=" + UPDATED_CONTEXT);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByContextIsInShouldWork() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where context in DEFAULT_CONTEXT or UPDATED_CONTEXT
        defaultFeedBackShouldBeFound("context.in=" + DEFAULT_CONTEXT + "," + UPDATED_CONTEXT);

        // Get all the feedBackList where context equals to UPDATED_CONTEXT
        defaultFeedBackShouldNotBeFound("context.in=" + UPDATED_CONTEXT);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByContextIsNullOrNotNull() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where context is not null
        defaultFeedBackShouldBeFound("context.specified=true");

        // Get all the feedBackList where context is null
        defaultFeedBackShouldNotBeFound("context.specified=false");
    }
                @Test
    @Transactional
    public void getAllFeedBacksByContextContainsSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where context contains DEFAULT_CONTEXT
        defaultFeedBackShouldBeFound("context.contains=" + DEFAULT_CONTEXT);

        // Get all the feedBackList where context contains UPDATED_CONTEXT
        defaultFeedBackShouldNotBeFound("context.contains=" + UPDATED_CONTEXT);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByContextNotContainsSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where context does not contain DEFAULT_CONTEXT
        defaultFeedBackShouldNotBeFound("context.doesNotContain=" + DEFAULT_CONTEXT);

        // Get all the feedBackList where context does not contain UPDATED_CONTEXT
        defaultFeedBackShouldBeFound("context.doesNotContain=" + UPDATED_CONTEXT);
    }


    @Test
    @Transactional
    public void getAllFeedBacksByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where status equals to DEFAULT_STATUS
        defaultFeedBackShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the feedBackList where status equals to UPDATED_STATUS
        defaultFeedBackShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where status not equals to DEFAULT_STATUS
        defaultFeedBackShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the feedBackList where status not equals to UPDATED_STATUS
        defaultFeedBackShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultFeedBackShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the feedBackList where status equals to UPDATED_STATUS
        defaultFeedBackShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where status is not null
        defaultFeedBackShouldBeFound("status.specified=true");

        // Get all the feedBackList where status is null
        defaultFeedBackShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllFeedBacksByStatusContainsSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where status contains DEFAULT_STATUS
        defaultFeedBackShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the feedBackList where status contains UPDATED_STATUS
        defaultFeedBackShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where status does not contain DEFAULT_STATUS
        defaultFeedBackShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the feedBackList where status does not contain UPDATED_STATUS
        defaultFeedBackShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate equals to DEFAULT_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.equals=" + DEFAULT_FEED_DATE);

        // Get all the feedBackList where feedDate equals to UPDATED_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.equals=" + UPDATED_FEED_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate not equals to DEFAULT_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.notEquals=" + DEFAULT_FEED_DATE);

        // Get all the feedBackList where feedDate not equals to UPDATED_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.notEquals=" + UPDATED_FEED_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsInShouldWork() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate in DEFAULT_FEED_DATE or UPDATED_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.in=" + DEFAULT_FEED_DATE + "," + UPDATED_FEED_DATE);

        // Get all the feedBackList where feedDate equals to UPDATED_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.in=" + UPDATED_FEED_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate is not null
        defaultFeedBackShouldBeFound("feedDate.specified=true");

        // Get all the feedBackList where feedDate is null
        defaultFeedBackShouldNotBeFound("feedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate is greater than or equal to DEFAULT_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.greaterThanOrEqual=" + DEFAULT_FEED_DATE);

        // Get all the feedBackList where feedDate is greater than or equal to UPDATED_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.greaterThanOrEqual=" + UPDATED_FEED_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate is less than or equal to DEFAULT_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.lessThanOrEqual=" + DEFAULT_FEED_DATE);

        // Get all the feedBackList where feedDate is less than or equal to SMALLER_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.lessThanOrEqual=" + SMALLER_FEED_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate is less than DEFAULT_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.lessThan=" + DEFAULT_FEED_DATE);

        // Get all the feedBackList where feedDate is less than UPDATED_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.lessThan=" + UPDATED_FEED_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFeedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where feedDate is greater than DEFAULT_FEED_DATE
        defaultFeedBackShouldNotBeFound("feedDate.greaterThan=" + DEFAULT_FEED_DATE);

        // Get all the feedBackList where feedDate is greater than SMALLER_FEED_DATE
        defaultFeedBackShouldBeFound("feedDate.greaterThan=" + SMALLER_FEED_DATE);
    }


    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate equals to DEFAULT_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.equals=" + DEFAULT_FIX_DATE);

        // Get all the feedBackList where fixDate equals to UPDATED_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.equals=" + UPDATED_FIX_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate not equals to DEFAULT_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.notEquals=" + DEFAULT_FIX_DATE);

        // Get all the feedBackList where fixDate not equals to UPDATED_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.notEquals=" + UPDATED_FIX_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsInShouldWork() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate in DEFAULT_FIX_DATE or UPDATED_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.in=" + DEFAULT_FIX_DATE + "," + UPDATED_FIX_DATE);

        // Get all the feedBackList where fixDate equals to UPDATED_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.in=" + UPDATED_FIX_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate is not null
        defaultFeedBackShouldBeFound("fixDate.specified=true");

        // Get all the feedBackList where fixDate is null
        defaultFeedBackShouldNotBeFound("fixDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate is greater than or equal to DEFAULT_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.greaterThanOrEqual=" + DEFAULT_FIX_DATE);

        // Get all the feedBackList where fixDate is greater than or equal to UPDATED_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.greaterThanOrEqual=" + UPDATED_FIX_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate is less than or equal to DEFAULT_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.lessThanOrEqual=" + DEFAULT_FIX_DATE);

        // Get all the feedBackList where fixDate is less than or equal to SMALLER_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.lessThanOrEqual=" + SMALLER_FIX_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsLessThanSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate is less than DEFAULT_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.lessThan=" + DEFAULT_FIX_DATE);

        // Get all the feedBackList where fixDate is less than UPDATED_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.lessThan=" + UPDATED_FIX_DATE);
    }

    @Test
    @Transactional
    public void getAllFeedBacksByFixDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList where fixDate is greater than DEFAULT_FIX_DATE
        defaultFeedBackShouldNotBeFound("fixDate.greaterThan=" + DEFAULT_FIX_DATE);

        // Get all the feedBackList where fixDate is greater than SMALLER_FIX_DATE
        defaultFeedBackShouldBeFound("fixDate.greaterThan=" + SMALLER_FIX_DATE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFeedBackShouldBeFound(String filter) throws Exception {
        restFeedBackMockMvc.perform(get("/api/feed-backs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feedBack.getId().intValue())))
            .andExpect(jsonPath("$.[*].context").value(hasItem(DEFAULT_CONTEXT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].feedDate").value(hasItem(DEFAULT_FEED_DATE.toString())))
            .andExpect(jsonPath("$.[*].fixDate").value(hasItem(DEFAULT_FIX_DATE.toString())));

        // Check, that the count call also returns 1
        restFeedBackMockMvc.perform(get("/api/feed-backs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFeedBackShouldNotBeFound(String filter) throws Exception {
        restFeedBackMockMvc.perform(get("/api/feed-backs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFeedBackMockMvc.perform(get("/api/feed-backs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingFeedBack() throws Exception {
        // Get the feedBack
        restFeedBackMockMvc.perform(get("/api/feed-backs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFeedBack() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        int databaseSizeBeforeUpdate = feedBackRepository.findAll().size();

        // Update the feedBack
        FeedBack updatedFeedBack = feedBackRepository.findById(feedBack.getId()).get();
        // Disconnect from session so that the updates on updatedFeedBack are not directly saved in db
        em.detach(updatedFeedBack);
        updatedFeedBack
            .context(UPDATED_CONTEXT)
            .status(UPDATED_STATUS)
            .feedDate(UPDATED_FEED_DATE)
            .fixDate(UPDATED_FIX_DATE);
        FeedBackDTO feedBackDTO = feedBackMapper.toDto(updatedFeedBack);

        restFeedBackMockMvc.perform(put("/api/feed-backs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedBackDTO)))
            .andExpect(status().isOk());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeUpdate);
        FeedBack testFeedBack = feedBackList.get(feedBackList.size() - 1);
        assertThat(testFeedBack.getContext()).isEqualTo(UPDATED_CONTEXT);
        assertThat(testFeedBack.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testFeedBack.getFeedDate()).isEqualTo(UPDATED_FEED_DATE);
        assertThat(testFeedBack.getFixDate()).isEqualTo(UPDATED_FIX_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingFeedBack() throws Exception {
        int databaseSizeBeforeUpdate = feedBackRepository.findAll().size();

        // Create the FeedBack
        FeedBackDTO feedBackDTO = feedBackMapper.toDto(feedBack);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeedBackMockMvc.perform(put("/api/feed-backs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedBackDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFeedBack() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        int databaseSizeBeforeDelete = feedBackRepository.findAll().size();

        // Delete the feedBack
        restFeedBackMockMvc.perform(delete("/api/feed-backs/{id}", feedBack.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeedBack.class);
        FeedBack feedBack1 = new FeedBack();
        feedBack1.setId(1L);
        FeedBack feedBack2 = new FeedBack();
        feedBack2.setId(feedBack1.getId());
        assertThat(feedBack1).isEqualTo(feedBack2);
        feedBack2.setId(2L);
        assertThat(feedBack1).isNotEqualTo(feedBack2);
        feedBack1.setId(null);
        assertThat(feedBack1).isNotEqualTo(feedBack2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeedBackDTO.class);
        FeedBackDTO feedBackDTO1 = new FeedBackDTO();
        feedBackDTO1.setId(1L);
        FeedBackDTO feedBackDTO2 = new FeedBackDTO();
        assertThat(feedBackDTO1).isNotEqualTo(feedBackDTO2);
        feedBackDTO2.setId(feedBackDTO1.getId());
        assertThat(feedBackDTO1).isEqualTo(feedBackDTO2);
        feedBackDTO2.setId(2L);
        assertThat(feedBackDTO1).isNotEqualTo(feedBackDTO2);
        feedBackDTO1.setId(null);
        assertThat(feedBackDTO1).isNotEqualTo(feedBackDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(feedBackMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(feedBackMapper.fromId(null)).isNull();
    }
}
