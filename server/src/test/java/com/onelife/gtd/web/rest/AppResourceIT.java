package com.onelife.gtd.web.rest;

import com.onelife.gtd.OnelifeApp;
import com.onelife.gtd.domain.App;
import com.onelife.gtd.repository.AppRepository;
import com.onelife.gtd.service.AppService;
import com.onelife.gtd.service.dto.AppDTO;
import com.onelife.gtd.service.mapper.AppMapper;
import com.onelife.gtd.web.rest.errors.ExceptionTranslator;
import com.onelife.gtd.service.dto.AppCriteria;
import com.onelife.gtd.service.AppQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.onelife.gtd.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AppResource} REST controller.
 */
@SpringBootTest(classes = OnelifeApp.class)
public class AppResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private AppMapper appMapper;

    @Autowired
    private AppService appService;

    @Autowired
    private AppQueryService appQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAppMockMvc;

    private App app;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AppResource appResource = new AppResource(appService, appQueryService);
        this.restAppMockMvc = MockMvcBuilders.standaloneSetup(appResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static App createEntity(EntityManager em) {
        App app = new App()
            .name(DEFAULT_NAME)
            .version(DEFAULT_VERSION);
        return app;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static App createUpdatedEntity(EntityManager em) {
        App app = new App()
            .name(UPDATED_NAME)
            .version(UPDATED_VERSION);
        return app;
    }

    @BeforeEach
    public void initTest() {
        app = createEntity(em);
    }

    @Test
    @Transactional
    public void createApp() throws Exception {
        int databaseSizeBeforeCreate = appRepository.findAll().size();

        // Create the App
        AppDTO appDTO = appMapper.toDto(app);
        restAppMockMvc.perform(post("/api/apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appDTO)))
            .andExpect(status().isCreated());

        // Validate the App in the database
        List<App> appList = appRepository.findAll();
        assertThat(appList).hasSize(databaseSizeBeforeCreate + 1);
        App testApp = appList.get(appList.size() - 1);
        assertThat(testApp.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testApp.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    public void createAppWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = appRepository.findAll().size();

        // Create the App with an existing ID
        app.setId(1L);
        AppDTO appDTO = appMapper.toDto(app);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAppMockMvc.perform(post("/api/apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appDTO)))
            .andExpect(status().isBadRequest());

        // Validate the App in the database
        List<App> appList = appRepository.findAll();
        assertThat(appList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllApps() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList
        restAppMockMvc.perform(get("/api/apps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(app.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)));
    }
    
    @Test
    @Transactional
    public void getApp() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get the app
        restAppMockMvc.perform(get("/api/apps/{id}", app.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(app.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION));
    }

    @Test
    @Transactional
    public void getAllAppsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where name equals to DEFAULT_NAME
        defaultAppShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the appList where name equals to UPDATED_NAME
        defaultAppShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAppsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where name not equals to DEFAULT_NAME
        defaultAppShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the appList where name not equals to UPDATED_NAME
        defaultAppShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAppsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where name in DEFAULT_NAME or UPDATED_NAME
        defaultAppShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the appList where name equals to UPDATED_NAME
        defaultAppShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAppsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where name is not null
        defaultAppShouldBeFound("name.specified=true");

        // Get all the appList where name is null
        defaultAppShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllAppsByNameContainsSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where name contains DEFAULT_NAME
        defaultAppShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the appList where name contains UPDATED_NAME
        defaultAppShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAppsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where name does not contain DEFAULT_NAME
        defaultAppShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the appList where name does not contain UPDATED_NAME
        defaultAppShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllAppsByVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where version equals to DEFAULT_VERSION
        defaultAppShouldBeFound("version.equals=" + DEFAULT_VERSION);

        // Get all the appList where version equals to UPDATED_VERSION
        defaultAppShouldNotBeFound("version.equals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllAppsByVersionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where version not equals to DEFAULT_VERSION
        defaultAppShouldNotBeFound("version.notEquals=" + DEFAULT_VERSION);

        // Get all the appList where version not equals to UPDATED_VERSION
        defaultAppShouldBeFound("version.notEquals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllAppsByVersionIsInShouldWork() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where version in DEFAULT_VERSION or UPDATED_VERSION
        defaultAppShouldBeFound("version.in=" + DEFAULT_VERSION + "," + UPDATED_VERSION);

        // Get all the appList where version equals to UPDATED_VERSION
        defaultAppShouldNotBeFound("version.in=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllAppsByVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where version is not null
        defaultAppShouldBeFound("version.specified=true");

        // Get all the appList where version is null
        defaultAppShouldNotBeFound("version.specified=false");
    }
                @Test
    @Transactional
    public void getAllAppsByVersionContainsSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where version contains DEFAULT_VERSION
        defaultAppShouldBeFound("version.contains=" + DEFAULT_VERSION);

        // Get all the appList where version contains UPDATED_VERSION
        defaultAppShouldNotBeFound("version.contains=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllAppsByVersionNotContainsSomething() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        // Get all the appList where version does not contain DEFAULT_VERSION
        defaultAppShouldNotBeFound("version.doesNotContain=" + DEFAULT_VERSION);

        // Get all the appList where version does not contain UPDATED_VERSION
        defaultAppShouldBeFound("version.doesNotContain=" + UPDATED_VERSION);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAppShouldBeFound(String filter) throws Exception {
        restAppMockMvc.perform(get("/api/apps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(app.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)));

        // Check, that the count call also returns 1
        restAppMockMvc.perform(get("/api/apps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAppShouldNotBeFound(String filter) throws Exception {
        restAppMockMvc.perform(get("/api/apps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAppMockMvc.perform(get("/api/apps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingApp() throws Exception {
        // Get the app
        restAppMockMvc.perform(get("/api/apps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateApp() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        int databaseSizeBeforeUpdate = appRepository.findAll().size();

        // Update the app
        App updatedApp = appRepository.findById(app.getId()).get();
        // Disconnect from session so that the updates on updatedApp are not directly saved in db
        em.detach(updatedApp);
        updatedApp
            .name(UPDATED_NAME)
            .version(UPDATED_VERSION);
        AppDTO appDTO = appMapper.toDto(updatedApp);

        restAppMockMvc.perform(put("/api/apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appDTO)))
            .andExpect(status().isOk());

        // Validate the App in the database
        List<App> appList = appRepository.findAll();
        assertThat(appList).hasSize(databaseSizeBeforeUpdate);
        App testApp = appList.get(appList.size() - 1);
        assertThat(testApp.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testApp.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void updateNonExistingApp() throws Exception {
        int databaseSizeBeforeUpdate = appRepository.findAll().size();

        // Create the App
        AppDTO appDTO = appMapper.toDto(app);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAppMockMvc.perform(put("/api/apps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appDTO)))
            .andExpect(status().isBadRequest());

        // Validate the App in the database
        List<App> appList = appRepository.findAll();
        assertThat(appList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteApp() throws Exception {
        // Initialize the database
        appRepository.saveAndFlush(app);

        int databaseSizeBeforeDelete = appRepository.findAll().size();

        // Delete the app
        restAppMockMvc.perform(delete("/api/apps/{id}", app.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<App> appList = appRepository.findAll();
        assertThat(appList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(App.class);
        App app1 = new App();
        app1.setId(1L);
        App app2 = new App();
        app2.setId(app1.getId());
        assertThat(app1).isEqualTo(app2);
        app2.setId(2L);
        assertThat(app1).isNotEqualTo(app2);
        app1.setId(null);
        assertThat(app1).isNotEqualTo(app2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AppDTO.class);
        AppDTO appDTO1 = new AppDTO();
        appDTO1.setId(1L);
        AppDTO appDTO2 = new AppDTO();
        assertThat(appDTO1).isNotEqualTo(appDTO2);
        appDTO2.setId(appDTO1.getId());
        assertThat(appDTO1).isEqualTo(appDTO2);
        appDTO2.setId(2L);
        assertThat(appDTO1).isNotEqualTo(appDTO2);
        appDTO1.setId(null);
        assertThat(appDTO1).isNotEqualTo(appDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(appMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(appMapper.fromId(null)).isNull();
    }
}
