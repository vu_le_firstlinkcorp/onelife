package com.onelife.gtd.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A References.
 */
@Entity
@Table(name = "jhi_references")
public class References implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "notes")
    private String notes;

    @ManyToMany(mappedBy = "references")
    @JsonIgnore
    private Set<Items> items = new HashSet<>();

    @ManyToMany(mappedBy = "references")
    @JsonIgnore
    private Set<Projecs> projecs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public References name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public References notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Set<Items> getItems() {
        return items;
    }

    public References items(Set<Items> items) {
        this.items = items;
        return this;
    }

    public References addItems(Items items) {
        this.items.add(items);
        items.getReferences().add(this);
        return this;
    }

    public References removeItems(Items items) {
        this.items.remove(items);
        items.getReferences().remove(this);
        return this;
    }

    public void setItems(Set<Items> items) {
        this.items = items;
    }

    public Set<Projecs> getProjecs() {
        return projecs;
    }

    public References projecs(Set<Projecs> projecs) {
        this.projecs = projecs;
        return this;
    }

    public References addProjecs(Projecs projecs) {
        this.projecs.add(projecs);
        projecs.getReferences().add(this);
        return this;
    }

    public References removeProjecs(Projecs projecs) {
        this.projecs.remove(projecs);
        projecs.getReferences().remove(this);
        return this;
    }

    public void setProjecs(Set<Projecs> projecs) {
        this.projecs = projecs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof References)) {
            return false;
        }
        return id != null && id.equals(((References) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "References{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
