package com.onelife.gtd.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Areas.
 */
@Entity
@Table(name = "areas")
public class Areas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JsonIgnoreProperties("areas")
    private Items items;

    @ManyToOne
    @JsonIgnoreProperties("areas")
    private Projecs projecs;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Areas name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Items getItems() {
        return items;
    }

    public Areas items(Items items) {
        this.items = items;
        return this;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    public Projecs getProjecs() {
        return projecs;
    }

    public Areas projecs(Projecs projecs) {
        this.projecs = projecs;
        return this;
    }

    public void setProjecs(Projecs projecs) {
        this.projecs = projecs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Areas)) {
            return false;
        }
        return id != null && id.equals(((Areas) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Areas{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
