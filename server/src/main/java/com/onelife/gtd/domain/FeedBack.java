package com.onelife.gtd.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A FeedBack.
 */
@Entity
@Table(name = "feed_back")
public class FeedBack implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "context")
    private String context;

    @Column(name = "status")
    private String status;

    @Column(name = "feed_date")
    private LocalDate feedDate;

    @Column(name = "fix_date")
    private LocalDate fixDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public FeedBack context(String context) {
        this.context = context;
        return this;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getStatus() {
        return status;
    }

    public FeedBack status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getFeedDate() {
        return feedDate;
    }

    public FeedBack feedDate(LocalDate feedDate) {
        this.feedDate = feedDate;
        return this;
    }

    public void setFeedDate(LocalDate feedDate) {
        this.feedDate = feedDate;
    }

    public LocalDate getFixDate() {
        return fixDate;
    }

    public FeedBack fixDate(LocalDate fixDate) {
        this.fixDate = fixDate;
        return this;
    }

    public void setFixDate(LocalDate fixDate) {
        this.fixDate = fixDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FeedBack)) {
            return false;
        }
        return id != null && id.equals(((FeedBack) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FeedBack{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            ", status='" + getStatus() + "'" +
            ", feedDate='" + getFeedDate() + "'" +
            ", fixDate='" + getFixDate() + "'" +
            "}";
    }
}
