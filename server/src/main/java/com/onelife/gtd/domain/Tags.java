package com.onelife.gtd.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Tags.
 */
@Entity
@Table(name = "tags")
public class Tags implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "context")
    private String context;

    @ManyToMany(mappedBy = "tags")
    @JsonIgnore
    private Set<Items> items = new HashSet<>();

    @ManyToMany(mappedBy = "tags")
    @JsonIgnore
    private Set<Projecs> projecs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public Tags context(String context) {
        this.context = context;
        return this;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Set<Items> getItems() {
        return items;
    }

    public Tags items(Set<Items> items) {
        this.items = items;
        return this;
    }

    public Tags addItems(Items items) {
        this.items.add(items);
        items.getTags().add(this);
        return this;
    }

    public Tags removeItems(Items items) {
        this.items.remove(items);
        items.getTags().remove(this);
        return this;
    }

    public void setItems(Set<Items> items) {
        this.items = items;
    }

    public Set<Projecs> getProjecs() {
        return projecs;
    }

    public Tags projecs(Set<Projecs> projecs) {
        this.projecs = projecs;
        return this;
    }

    public Tags addProjecs(Projecs projecs) {
        this.projecs.add(projecs);
        projecs.getTags().add(this);
        return this;
    }

    public Tags removeProjecs(Projecs projecs) {
        this.projecs.remove(projecs);
        projecs.getTags().remove(this);
        return this;
    }

    public void setProjecs(Set<Projecs> projecs) {
        this.projecs = projecs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tags)) {
            return false;
        }
        return id != null && id.equals(((Tags) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Tags{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            "}";
    }
}
