package com.onelife.gtd.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Projecs.
 */
@Entity
@Table(name = "projecs")
public class Projecs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "notes")
    private String notes;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @Column(name = "project_type")
    private String projectType;

    @Column(name = "focus")
    private String focus;

    @Column(name = "status")
    private String status;

    @ManyToOne
    @JsonIgnoreProperties("projecs")
    private Items items;

    @OneToMany(mappedBy = "projecs")
    private Set<Contacts> contacts = new HashSet<>();

    @OneToMany(mappedBy = "projecs")
    private Set<Areas> areas = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "projecs_tags",
               joinColumns = @JoinColumn(name = "projecs_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tags_id", referencedColumnName = "id"))
    private Set<Tags> tags = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "projecs_references",
               joinColumns = @JoinColumn(name = "projecs_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "references_id", referencedColumnName = "id"))
    private Set<References> references = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Projecs name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public Projecs notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public Projecs dueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getProjectType() {
        return projectType;
    }

    public Projecs projectType(String projectType) {
        this.projectType = projectType;
        return this;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getFocus() {
        return focus;
    }

    public Projecs focus(String focus) {
        this.focus = focus;
        return this;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getStatus() {
        return status;
    }

    public Projecs status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Items getItems() {
        return items;
    }

    public Projecs items(Items items) {
        this.items = items;
        return this;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    public Set<Contacts> getContacts() {
        return contacts;
    }

    public Projecs contacts(Set<Contacts> contacts) {
        this.contacts = contacts;
        return this;
    }

    public Projecs addContacts(Contacts contacts) {
        this.contacts.add(contacts);
        contacts.setProjecs(this);
        return this;
    }

    public Projecs removeContacts(Contacts contacts) {
        this.contacts.remove(contacts);
        contacts.setProjecs(null);
        return this;
    }

    public void setContacts(Set<Contacts> contacts) {
        this.contacts = contacts;
    }

    public Set<Areas> getAreas() {
        return areas;
    }

    public Projecs areas(Set<Areas> areas) {
        this.areas = areas;
        return this;
    }

    public Projecs addAreas(Areas areas) {
        this.areas.add(areas);
        areas.setProjecs(this);
        return this;
    }

    public Projecs removeAreas(Areas areas) {
        this.areas.remove(areas);
        areas.setProjecs(null);
        return this;
    }

    public void setAreas(Set<Areas> areas) {
        this.areas = areas;
    }

    public Set<Tags> getTags() {
        return tags;
    }

    public Projecs tags(Set<Tags> tags) {
        this.tags = tags;
        return this;
    }

    public Projecs addTags(Tags tags) {
        this.tags.add(tags);
        tags.getProjecs().add(this);
        return this;
    }

    public Projecs removeTags(Tags tags) {
        this.tags.remove(tags);
        tags.getProjecs().remove(this);
        return this;
    }

    public void setTags(Set<Tags> tags) {
        this.tags = tags;
    }

    public Set<References> getReferences() {
        return references;
    }

    public Projecs references(Set<References> references) {
        this.references = references;
        return this;
    }

    public Projecs addReferences(References references) {
        this.references.add(references);
        references.getProjecs().add(this);
        return this;
    }

    public Projecs removeReferences(References references) {
        this.references.remove(references);
        references.getProjecs().remove(this);
        return this;
    }

    public void setReferences(Set<References> references) {
        this.references = references;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Projecs)) {
            return false;
        }
        return id != null && id.equals(((Projecs) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Projecs{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", notes='" + getNotes() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", projectType='" + getProjectType() + "'" +
            ", focus='" + getFocus() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
