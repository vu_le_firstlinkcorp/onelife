package com.onelife.gtd.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Items.
 */
@Entity
@Table(name = "items")
public class Items implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "to_do")
    private String toDo;

    @Column(name = "notes")
    private String notes;

    @Column(name = "time")
    private String time;

    @Column(name = "energy")
    private String energy;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @Column(name = "item_type")
    private String itemType;

    @Column(name = "focus")
    private String focus;

    @Column(name = "status")
    private String status;

    @OneToMany(mappedBy = "items")
    private Set<Contacts> contacts = new HashSet<>();

    @OneToMany(mappedBy = "items")
    private Set<Areas> areas = new HashSet<>();

    @OneToMany(mappedBy = "items")
    private Set<Projecs> projecs = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "items_tags",
               joinColumns = @JoinColumn(name = "items_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tags_id", referencedColumnName = "id"))
    private Set<Tags> tags = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "items_references",
               joinColumns = @JoinColumn(name = "items_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "references_id", referencedColumnName = "id"))
    private Set<References> references = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToDo() {
        return toDo;
    }

    public Items toDo(String toDo) {
        this.toDo = toDo;
        return this;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }

    public String getNotes() {
        return notes;
    }

    public Items notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTime() {
        return time;
    }

    public Items time(String time) {
        this.time = time;
        return this;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEnergy() {
        return energy;
    }

    public Items energy(String energy) {
        this.energy = energy;
        return this;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public Items dueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getItemType() {
        return itemType;
    }

    public Items itemType(String itemType) {
        this.itemType = itemType;
        return this;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getFocus() {
        return focus;
    }

    public Items focus(String focus) {
        this.focus = focus;
        return this;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getStatus() {
        return status;
    }

    public Items status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Contacts> getContacts() {
        return contacts;
    }

    public Items contacts(Set<Contacts> contacts) {
        this.contacts = contacts;
        return this;
    }

    public Items addContacts(Contacts contacts) {
        this.contacts.add(contacts);
        contacts.setItems(this);
        return this;
    }

    public Items removeContacts(Contacts contacts) {
        this.contacts.remove(contacts);
        contacts.setItems(null);
        return this;
    }

    public void setContacts(Set<Contacts> contacts) {
        this.contacts = contacts;
    }

    public Set<Areas> getAreas() {
        return areas;
    }

    public Items areas(Set<Areas> areas) {
        this.areas = areas;
        return this;
    }

    public Items addAreas(Areas areas) {
        this.areas.add(areas);
        areas.setItems(this);
        return this;
    }

    public Items removeAreas(Areas areas) {
        this.areas.remove(areas);
        areas.setItems(null);
        return this;
    }

    public void setAreas(Set<Areas> areas) {
        this.areas = areas;
    }

    public Set<Projecs> getProjecs() {
        return projecs;
    }

    public Items projecs(Set<Projecs> projecs) {
        this.projecs = projecs;
        return this;
    }

    public Items addProjecs(Projecs projecs) {
        this.projecs.add(projecs);
        projecs.setItems(this);
        return this;
    }

    public Items removeProjecs(Projecs projecs) {
        this.projecs.remove(projecs);
        projecs.setItems(null);
        return this;
    }

    public void setProjecs(Set<Projecs> projecs) {
        this.projecs = projecs;
    }

    public Set<Tags> getTags() {
        return tags;
    }

    public Items tags(Set<Tags> tags) {
        this.tags = tags;
        return this;
    }

    public Items addTags(Tags tags) {
        this.tags.add(tags);
        tags.getItems().add(this);
        return this;
    }

    public Items removeTags(Tags tags) {
        this.tags.remove(tags);
        tags.getItems().remove(this);
        return this;
    }

    public void setTags(Set<Tags> tags) {
        this.tags = tags;
    }

    public Set<References> getReferences() {
        return references;
    }

    public Items references(Set<References> references) {
        this.references = references;
        return this;
    }

    public Items addReferences(References references) {
        this.references.add(references);
        references.getItems().add(this);
        return this;
    }

    public Items removeReferences(References references) {
        this.references.remove(references);
        references.getItems().remove(this);
        return this;
    }

    public void setReferences(Set<References> references) {
        this.references = references;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Items)) {
            return false;
        }
        return id != null && id.equals(((Items) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Items{" +
            "id=" + getId() +
            ", toDo='" + getToDo() + "'" +
            ", notes='" + getNotes() + "'" +
            ", time='" + getTime() + "'" +
            ", energy='" + getEnergy() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", itemType='" + getItemType() + "'" +
            ", focus='" + getFocus() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
