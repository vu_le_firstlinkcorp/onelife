package com.onelife.gtd.repository;
import com.onelife.gtd.domain.Projecs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Projecs entity.
 */
@Repository
public interface ProjecsRepository extends JpaRepository<Projecs, Long>, JpaSpecificationExecutor<Projecs> {

    @Query(value = "select distinct projecs from Projecs projecs left join fetch projecs.tags left join fetch projecs.references",
        countQuery = "select count(distinct projecs) from Projecs projecs")
    Page<Projecs> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct projecs from Projecs projecs left join fetch projecs.tags left join fetch projecs.references")
    List<Projecs> findAllWithEagerRelationships();

    @Query("select projecs from Projecs projecs left join fetch projecs.tags left join fetch projecs.references where projecs.id =:id")
    Optional<Projecs> findOneWithEagerRelationships(@Param("id") Long id);

}
