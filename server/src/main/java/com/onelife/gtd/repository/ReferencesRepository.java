package com.onelife.gtd.repository;
import com.onelife.gtd.domain.References;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the References entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReferencesRepository extends JpaRepository<References, Long>, JpaSpecificationExecutor<References> {

}
