package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.TagsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tags} and its DTO {@link TagsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TagsMapper extends EntityMapper<TagsDTO, Tags> {


    @Mapping(target = "items", ignore = true)
    @Mapping(target = "removeItems", ignore = true)
    @Mapping(target = "projecs", ignore = true)
    @Mapping(target = "removeProjecs", ignore = true)
    Tags toEntity(TagsDTO tagsDTO);

    default Tags fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tags tags = new Tags();
        tags.setId(id);
        return tags;
    }
}
