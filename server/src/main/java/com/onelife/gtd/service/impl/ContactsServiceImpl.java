package com.onelife.gtd.service.impl;

import com.onelife.gtd.service.ContactsService;
import com.onelife.gtd.domain.Contacts;
import com.onelife.gtd.repository.ContactsRepository;
import com.onelife.gtd.service.dto.ContactsDTO;
import com.onelife.gtd.service.mapper.ContactsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Contacts}.
 */
@Service
@Transactional
public class ContactsServiceImpl implements ContactsService {

    private final Logger log = LoggerFactory.getLogger(ContactsServiceImpl.class);

    private final ContactsRepository contactsRepository;

    private final ContactsMapper contactsMapper;

    public ContactsServiceImpl(ContactsRepository contactsRepository, ContactsMapper contactsMapper) {
        this.contactsRepository = contactsRepository;
        this.contactsMapper = contactsMapper;
    }

    /**
     * Save a contacts.
     *
     * @param contactsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ContactsDTO save(ContactsDTO contactsDTO) {
        log.debug("Request to save Contacts : {}", contactsDTO);
        Contacts contacts = contactsMapper.toEntity(contactsDTO);
        contacts = contactsRepository.save(contacts);
        return contactsMapper.toDto(contacts);
    }

    /**
     * Get all the contacts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ContactsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contacts");
        return contactsRepository.findAll(pageable)
            .map(contactsMapper::toDto);
    }


    /**
     * Get one contacts by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ContactsDTO> findOne(Long id) {
        log.debug("Request to get Contacts : {}", id);
        return contactsRepository.findById(id)
            .map(contactsMapper::toDto);
    }

    /**
     * Delete the contacts by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Contacts : {}", id);
        contactsRepository.deleteById(id);
    }
}
