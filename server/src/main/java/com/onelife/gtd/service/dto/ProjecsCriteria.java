package com.onelife.gtd.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.onelife.gtd.domain.Projecs} entity. This class is used
 * in {@link com.onelife.gtd.web.rest.ProjecsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /projecs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProjecsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter notes;

    private LocalDateFilter dueDate;

    private StringFilter projectType;

    private StringFilter focus;

    private StringFilter status;

    private LongFilter itemsId;

    private LongFilter contactsId;

    private LongFilter areasId;

    private LongFilter tagsId;

    private LongFilter referencesId;

    public ProjecsCriteria(){
    }

    public ProjecsCriteria(ProjecsCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.dueDate = other.dueDate == null ? null : other.dueDate.copy();
        this.projectType = other.projectType == null ? null : other.projectType.copy();
        this.focus = other.focus == null ? null : other.focus.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.itemsId = other.itemsId == null ? null : other.itemsId.copy();
        this.contactsId = other.contactsId == null ? null : other.contactsId.copy();
        this.areasId = other.areasId == null ? null : other.areasId.copy();
        this.tagsId = other.tagsId == null ? null : other.tagsId.copy();
        this.referencesId = other.referencesId == null ? null : other.referencesId.copy();
    }

    @Override
    public ProjecsCriteria copy() {
        return new ProjecsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public LocalDateFilter getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateFilter dueDate) {
        this.dueDate = dueDate;
    }

    public StringFilter getProjectType() {
        return projectType;
    }

    public void setProjectType(StringFilter projectType) {
        this.projectType = projectType;
    }

    public StringFilter getFocus() {
        return focus;
    }

    public void setFocus(StringFilter focus) {
        this.focus = focus;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public LongFilter getItemsId() {
        return itemsId;
    }

    public void setItemsId(LongFilter itemsId) {
        this.itemsId = itemsId;
    }

    public LongFilter getContactsId() {
        return contactsId;
    }

    public void setContactsId(LongFilter contactsId) {
        this.contactsId = contactsId;
    }

    public LongFilter getAreasId() {
        return areasId;
    }

    public void setAreasId(LongFilter areasId) {
        this.areasId = areasId;
    }

    public LongFilter getTagsId() {
        return tagsId;
    }

    public void setTagsId(LongFilter tagsId) {
        this.tagsId = tagsId;
    }

    public LongFilter getReferencesId() {
        return referencesId;
    }

    public void setReferencesId(LongFilter referencesId) {
        this.referencesId = referencesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProjecsCriteria that = (ProjecsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(dueDate, that.dueDate) &&
            Objects.equals(projectType, that.projectType) &&
            Objects.equals(focus, that.focus) &&
            Objects.equals(status, that.status) &&
            Objects.equals(itemsId, that.itemsId) &&
            Objects.equals(contactsId, that.contactsId) &&
            Objects.equals(areasId, that.areasId) &&
            Objects.equals(tagsId, that.tagsId) &&
            Objects.equals(referencesId, that.referencesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        notes,
        dueDate,
        projectType,
        focus,
        status,
        itemsId,
        contactsId,
        areasId,
        tagsId,
        referencesId
        );
    }

    @Override
    public String toString() {
        return "ProjecsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (dueDate != null ? "dueDate=" + dueDate + ", " : "") +
                (projectType != null ? "projectType=" + projectType + ", " : "") +
                (focus != null ? "focus=" + focus + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (itemsId != null ? "itemsId=" + itemsId + ", " : "") +
                (contactsId != null ? "contactsId=" + contactsId + ", " : "") +
                (areasId != null ? "areasId=" + areasId + ", " : "") +
                (tagsId != null ? "tagsId=" + tagsId + ", " : "") +
                (referencesId != null ? "referencesId=" + referencesId + ", " : "") +
            "}";
    }

}
