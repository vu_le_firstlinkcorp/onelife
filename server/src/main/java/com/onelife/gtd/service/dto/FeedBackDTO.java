package com.onelife.gtd.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.FeedBack} entity.
 */
public class FeedBackDTO implements Serializable {

    private Long id;

    private String context;

    private String status;

    private LocalDate feedDate;

    private LocalDate fixDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getFeedDate() {
        return feedDate;
    }

    public void setFeedDate(LocalDate feedDate) {
        this.feedDate = feedDate;
    }

    public LocalDate getFixDate() {
        return fixDate;
    }

    public void setFixDate(LocalDate fixDate) {
        this.fixDate = fixDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeedBackDTO feedBackDTO = (FeedBackDTO) o;
        if (feedBackDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), feedBackDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FeedBackDTO{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            ", status='" + getStatus() + "'" +
            ", feedDate='" + getFeedDate() + "'" +
            ", fixDate='" + getFixDate() + "'" +
            "}";
    }
}
