package com.onelife.gtd.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.onelife.gtd.domain.Tags;
import com.onelife.gtd.domain.*; // for static metamodels
import com.onelife.gtd.repository.TagsRepository;
import com.onelife.gtd.service.dto.TagsCriteria;
import com.onelife.gtd.service.dto.TagsDTO;
import com.onelife.gtd.service.mapper.TagsMapper;

/**
 * Service for executing complex queries for {@link Tags} entities in the database.
 * The main input is a {@link TagsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TagsDTO} or a {@link Page} of {@link TagsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TagsQueryService extends QueryService<Tags> {

    private final Logger log = LoggerFactory.getLogger(TagsQueryService.class);

    private final TagsRepository tagsRepository;

    private final TagsMapper tagsMapper;

    public TagsQueryService(TagsRepository tagsRepository, TagsMapper tagsMapper) {
        this.tagsRepository = tagsRepository;
        this.tagsMapper = tagsMapper;
    }

    /**
     * Return a {@link List} of {@link TagsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TagsDTO> findByCriteria(TagsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Tags> specification = createSpecification(criteria);
        return tagsMapper.toDto(tagsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TagsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TagsDTO> findByCriteria(TagsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Tags> specification = createSpecification(criteria);
        return tagsRepository.findAll(specification, page)
            .map(tagsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TagsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Tags> specification = createSpecification(criteria);
        return tagsRepository.count(specification);
    }

    /**
     * Function to convert {@link TagsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Tags> createSpecification(TagsCriteria criteria) {
        Specification<Tags> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Tags_.id));
            }
            if (criteria.getContext() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContext(), Tags_.context));
            }
            if (criteria.getItemsId() != null) {
                specification = specification.and(buildSpecification(criteria.getItemsId(),
                    root -> root.join(Tags_.items, JoinType.LEFT).get(Items_.id)));
            }
            if (criteria.getProjecsId() != null) {
                specification = specification.and(buildSpecification(criteria.getProjecsId(),
                    root -> root.join(Tags_.projecs, JoinType.LEFT).get(Projecs_.id)));
            }
        }
        return specification;
    }
}
