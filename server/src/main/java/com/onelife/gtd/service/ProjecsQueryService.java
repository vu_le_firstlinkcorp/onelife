package com.onelife.gtd.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.domain.*; // for static metamodels
import com.onelife.gtd.repository.ProjecsRepository;
import com.onelife.gtd.service.dto.ProjecsCriteria;
import com.onelife.gtd.service.dto.ProjecsDTO;
import com.onelife.gtd.service.mapper.ProjecsMapper;

/**
 * Service for executing complex queries for {@link Projecs} entities in the database.
 * The main input is a {@link ProjecsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProjecsDTO} or a {@link Page} of {@link ProjecsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProjecsQueryService extends QueryService<Projecs> {

    private final Logger log = LoggerFactory.getLogger(ProjecsQueryService.class);

    private final ProjecsRepository projecsRepository;

    private final ProjecsMapper projecsMapper;

    public ProjecsQueryService(ProjecsRepository projecsRepository, ProjecsMapper projecsMapper) {
        this.projecsRepository = projecsRepository;
        this.projecsMapper = projecsMapper;
    }

    /**
     * Return a {@link List} of {@link ProjecsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProjecsDTO> findByCriteria(ProjecsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Projecs> specification = createSpecification(criteria);
        return projecsMapper.toDto(projecsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProjecsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProjecsDTO> findByCriteria(ProjecsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Projecs> specification = createSpecification(criteria);
        return projecsRepository.findAll(specification, page)
            .map(projecsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProjecsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Projecs> specification = createSpecification(criteria);
        return projecsRepository.count(specification);
    }

    /**
     * Function to convert {@link ProjecsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Projecs> createSpecification(ProjecsCriteria criteria) {
        Specification<Projecs> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Projecs_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Projecs_.name));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Projecs_.notes));
            }
            if (criteria.getDueDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDueDate(), Projecs_.dueDate));
            }
            if (criteria.getProjectType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProjectType(), Projecs_.projectType));
            }
            if (criteria.getFocus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFocus(), Projecs_.focus));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Projecs_.status));
            }
            if (criteria.getItemsId() != null) {
                specification = specification.and(buildSpecification(criteria.getItemsId(),
                    root -> root.join(Projecs_.items, JoinType.LEFT).get(Items_.id)));
            }
            if (criteria.getContactsId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactsId(),
                    root -> root.join(Projecs_.contacts, JoinType.LEFT).get(Contacts_.id)));
            }
            if (criteria.getAreasId() != null) {
                specification = specification.and(buildSpecification(criteria.getAreasId(),
                    root -> root.join(Projecs_.areas, JoinType.LEFT).get(Areas_.id)));
            }
            if (criteria.getTagsId() != null) {
                specification = specification.and(buildSpecification(criteria.getTagsId(),
                    root -> root.join(Projecs_.tags, JoinType.LEFT).get(Tags_.id)));
            }
            if (criteria.getReferencesId() != null) {
                specification = specification.and(buildSpecification(criteria.getReferencesId(),
                    root -> root.join(Projecs_.references, JoinType.LEFT).get(References_.id)));
            }
        }
        return specification;
    }
}
