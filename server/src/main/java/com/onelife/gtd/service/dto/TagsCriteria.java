package com.onelife.gtd.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.onelife.gtd.domain.Tags} entity. This class is used
 * in {@link com.onelife.gtd.web.rest.TagsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tags?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TagsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter context;

    private LongFilter itemsId;

    private LongFilter projecsId;

    public TagsCriteria(){
    }

    public TagsCriteria(TagsCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.context = other.context == null ? null : other.context.copy();
        this.itemsId = other.itemsId == null ? null : other.itemsId.copy();
        this.projecsId = other.projecsId == null ? null : other.projecsId.copy();
    }

    @Override
    public TagsCriteria copy() {
        return new TagsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getContext() {
        return context;
    }

    public void setContext(StringFilter context) {
        this.context = context;
    }

    public LongFilter getItemsId() {
        return itemsId;
    }

    public void setItemsId(LongFilter itemsId) {
        this.itemsId = itemsId;
    }

    public LongFilter getProjecsId() {
        return projecsId;
    }

    public void setProjecsId(LongFilter projecsId) {
        this.projecsId = projecsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TagsCriteria that = (TagsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(context, that.context) &&
            Objects.equals(itemsId, that.itemsId) &&
            Objects.equals(projecsId, that.projecsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        context,
        itemsId,
        projecsId
        );
    }

    @Override
    public String toString() {
        return "TagsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (context != null ? "context=" + context + ", " : "") +
                (itemsId != null ? "itemsId=" + itemsId + ", " : "") +
                (projecsId != null ? "projecsId=" + projecsId + ", " : "") +
            "}";
    }

}
