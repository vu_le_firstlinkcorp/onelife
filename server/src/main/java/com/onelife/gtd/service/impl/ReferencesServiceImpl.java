package com.onelife.gtd.service.impl;

import com.onelife.gtd.service.ReferencesService;
import com.onelife.gtd.domain.References;
import com.onelife.gtd.repository.ReferencesRepository;
import com.onelife.gtd.service.dto.ReferencesDTO;
import com.onelife.gtd.service.mapper.ReferencesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link References}.
 */
@Service
@Transactional
public class ReferencesServiceImpl implements ReferencesService {

    private final Logger log = LoggerFactory.getLogger(ReferencesServiceImpl.class);

    private final ReferencesRepository referencesRepository;

    private final ReferencesMapper referencesMapper;

    public ReferencesServiceImpl(ReferencesRepository referencesRepository, ReferencesMapper referencesMapper) {
        this.referencesRepository = referencesRepository;
        this.referencesMapper = referencesMapper;
    }

    /**
     * Save a references.
     *
     * @param referencesDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ReferencesDTO save(ReferencesDTO referencesDTO) {
        log.debug("Request to save References : {}", referencesDTO);
        References references = referencesMapper.toEntity(referencesDTO);
        references = referencesRepository.save(references);
        return referencesMapper.toDto(references);
    }

    /**
     * Get all the references.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReferencesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all References");
        return referencesRepository.findAll(pageable)
            .map(referencesMapper::toDto);
    }


    /**
     * Get one references by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ReferencesDTO> findOne(Long id) {
        log.debug("Request to get References : {}", id);
        return referencesRepository.findById(id)
            .map(referencesMapper::toDto);
    }

    /**
     * Delete the references by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete References : {}", id);
        referencesRepository.deleteById(id);
    }
}
