package com.onelife.gtd.service;

import com.onelife.gtd.service.dto.ItemsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.onelife.gtd.domain.Items}.
 */
public interface ItemsService {

    /**
     * Save a items.
     *
     * @param itemsDTO the entity to save.
     * @return the persisted entity.
     */
    ItemsDTO save(ItemsDTO itemsDTO);

    /**
     * Get all the items.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ItemsDTO> findAll(Pageable pageable);

    /**
     * Get all the items with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<ItemsDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" items.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ItemsDTO> findOne(Long id);

    /**
     * Delete the "id" items.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
