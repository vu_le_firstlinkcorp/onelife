package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.AppDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link App} and its DTO {@link AppDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AppMapper extends EntityMapper<AppDTO, App> {



    default App fromId(Long id) {
        if (id == null) {
            return null;
        }
        App app = new App();
        app.setId(id);
        return app;
    }
}
