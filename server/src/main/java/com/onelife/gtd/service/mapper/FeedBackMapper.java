package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.FeedBackDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FeedBack} and its DTO {@link FeedBackDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FeedBackMapper extends EntityMapper<FeedBackDTO, FeedBack> {



    default FeedBack fromId(Long id) {
        if (id == null) {
            return null;
        }
        FeedBack feedBack = new FeedBack();
        feedBack.setId(id);
        return feedBack;
    }
}
