package com.onelife.gtd.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.Items} entity.
 */
public class ItemsDTO implements Serializable {

    private Long id;

    private String toDo;

    private String notes;

    private String time;

    private String energy;

    private LocalDate dueDate;

    private String itemType;

    private String focus;

    private String status;


    private Set<TagsDTO> tags = new HashSet<>();

    private Set<ReferencesDTO> references = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToDo() {
        return toDo;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<TagsDTO> getTags() {
        return tags;
    }

    public void setTags(Set<TagsDTO> tags) {
        this.tags = tags;
    }

    public Set<ReferencesDTO> getReferences() {
        return references;
    }

    public void setReferences(Set<ReferencesDTO> references) {
        this.references = references;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ItemsDTO itemsDTO = (ItemsDTO) o;
        if (itemsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), itemsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ItemsDTO{" +
            "id=" + getId() +
            ", toDo='" + getToDo() + "'" +
            ", notes='" + getNotes() + "'" +
            ", time='" + getTime() + "'" +
            ", energy='" + getEnergy() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", itemType='" + getItemType() + "'" +
            ", focus='" + getFocus() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
