package com.onelife.gtd.service.impl;

import com.onelife.gtd.service.ProjecsService;
import com.onelife.gtd.domain.Projecs;
import com.onelife.gtd.repository.ProjecsRepository;
import com.onelife.gtd.service.dto.ProjecsDTO;
import com.onelife.gtd.service.mapper.ProjecsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Projecs}.
 */
@Service
@Transactional
public class ProjecsServiceImpl implements ProjecsService {

    private final Logger log = LoggerFactory.getLogger(ProjecsServiceImpl.class);

    private final ProjecsRepository projecsRepository;

    private final ProjecsMapper projecsMapper;

    public ProjecsServiceImpl(ProjecsRepository projecsRepository, ProjecsMapper projecsMapper) {
        this.projecsRepository = projecsRepository;
        this.projecsMapper = projecsMapper;
    }

    /**
     * Save a projecs.
     *
     * @param projecsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProjecsDTO save(ProjecsDTO projecsDTO) {
        log.debug("Request to save Projecs : {}", projecsDTO);
        Projecs projecs = projecsMapper.toEntity(projecsDTO);
        projecs = projecsRepository.save(projecs);
        return projecsMapper.toDto(projecs);
    }

    /**
     * Get all the projecs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProjecsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Projecs");
        return projecsRepository.findAll(pageable)
            .map(projecsMapper::toDto);
    }

    /**
     * Get all the projecs with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<ProjecsDTO> findAllWithEagerRelationships(Pageable pageable) {
        return projecsRepository.findAllWithEagerRelationships(pageable).map(projecsMapper::toDto);
    }
    

    /**
     * Get one projecs by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProjecsDTO> findOne(Long id) {
        log.debug("Request to get Projecs : {}", id);
        return projecsRepository.findOneWithEagerRelationships(id)
            .map(projecsMapper::toDto);
    }

    /**
     * Delete the projecs by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Projecs : {}", id);
        projecsRepository.deleteById(id);
    }
}
