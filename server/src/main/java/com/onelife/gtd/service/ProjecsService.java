package com.onelife.gtd.service;

import com.onelife.gtd.service.dto.ProjecsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.onelife.gtd.domain.Projecs}.
 */
public interface ProjecsService {

    /**
     * Save a projecs.
     *
     * @param projecsDTO the entity to save.
     * @return the persisted entity.
     */
    ProjecsDTO save(ProjecsDTO projecsDTO);

    /**
     * Get all the projecs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProjecsDTO> findAll(Pageable pageable);

    /**
     * Get all the projecs with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<ProjecsDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" projecs.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProjecsDTO> findOne(Long id);

    /**
     * Delete the "id" projecs.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
