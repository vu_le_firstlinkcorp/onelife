package com.onelife.gtd.service.impl;

import com.onelife.gtd.service.ItemsService;
import com.onelife.gtd.domain.Items;
import com.onelife.gtd.repository.ItemsRepository;
import com.onelife.gtd.service.dto.ItemsDTO;
import com.onelife.gtd.service.mapper.ItemsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Items}.
 */
@Service
@Transactional
public class ItemsServiceImpl implements ItemsService {

    private final Logger log = LoggerFactory.getLogger(ItemsServiceImpl.class);

    private final ItemsRepository itemsRepository;

    private final ItemsMapper itemsMapper;

    public ItemsServiceImpl(ItemsRepository itemsRepository, ItemsMapper itemsMapper) {
        this.itemsRepository = itemsRepository;
        this.itemsMapper = itemsMapper;
    }

    /**
     * Save a items.
     *
     * @param itemsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ItemsDTO save(ItemsDTO itemsDTO) {
        log.debug("Request to save Items : {}", itemsDTO);
        Items items = itemsMapper.toEntity(itemsDTO);
        items = itemsRepository.save(items);
        return itemsMapper.toDto(items);
    }

    /**
     * Get all the items.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ItemsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Items");
        return itemsRepository.findAll(pageable)
            .map(itemsMapper::toDto);
    }

    /**
     * Get all the items with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<ItemsDTO> findAllWithEagerRelationships(Pageable pageable) {
        return itemsRepository.findAllWithEagerRelationships(pageable).map(itemsMapper::toDto);
    }
    

    /**
     * Get one items by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ItemsDTO> findOne(Long id) {
        log.debug("Request to get Items : {}", id);
        return itemsRepository.findOneWithEagerRelationships(id)
            .map(itemsMapper::toDto);
    }

    /**
     * Delete the items by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Items : {}", id);
        itemsRepository.deleteById(id);
    }
}
