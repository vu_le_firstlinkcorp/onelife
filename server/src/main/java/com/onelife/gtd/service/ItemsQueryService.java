package com.onelife.gtd.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.onelife.gtd.domain.Items;
import com.onelife.gtd.domain.*; // for static metamodels
import com.onelife.gtd.repository.ItemsRepository;
import com.onelife.gtd.service.dto.ItemsCriteria;
import com.onelife.gtd.service.dto.ItemsDTO;
import com.onelife.gtd.service.mapper.ItemsMapper;

/**
 * Service for executing complex queries for {@link Items} entities in the database.
 * The main input is a {@link ItemsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ItemsDTO} or a {@link Page} of {@link ItemsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ItemsQueryService extends QueryService<Items> {

    private final Logger log = LoggerFactory.getLogger(ItemsQueryService.class);

    private final ItemsRepository itemsRepository;

    private final ItemsMapper itemsMapper;

    public ItemsQueryService(ItemsRepository itemsRepository, ItemsMapper itemsMapper) {
        this.itemsRepository = itemsRepository;
        this.itemsMapper = itemsMapper;
    }

    /**
     * Return a {@link List} of {@link ItemsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ItemsDTO> findByCriteria(ItemsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Items> specification = createSpecification(criteria);
        return itemsMapper.toDto(itemsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ItemsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ItemsDTO> findByCriteria(ItemsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Items> specification = createSpecification(criteria);
        return itemsRepository.findAll(specification, page)
            .map(itemsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ItemsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Items> specification = createSpecification(criteria);
        return itemsRepository.count(specification);
    }

    /**
     * Function to convert {@link ItemsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Items> createSpecification(ItemsCriteria criteria) {
        Specification<Items> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Items_.id));
            }
            if (criteria.getToDo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getToDo(), Items_.toDo));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Items_.notes));
            }
            if (criteria.getTime() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTime(), Items_.time));
            }
            if (criteria.getEnergy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnergy(), Items_.energy));
            }
            if (criteria.getDueDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDueDate(), Items_.dueDate));
            }
            if (criteria.getItemType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemType(), Items_.itemType));
            }
            if (criteria.getFocus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFocus(), Items_.focus));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Items_.status));
            }
            if (criteria.getContactsId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactsId(),
                    root -> root.join(Items_.contacts, JoinType.LEFT).get(Contacts_.id)));
            }
            if (criteria.getAreasId() != null) {
                specification = specification.and(buildSpecification(criteria.getAreasId(),
                    root -> root.join(Items_.areas, JoinType.LEFT).get(Areas_.id)));
            }
            if (criteria.getProjecsId() != null) {
                specification = specification.and(buildSpecification(criteria.getProjecsId(),
                    root -> root.join(Items_.projecs, JoinType.LEFT).get(Projecs_.id)));
            }
            if (criteria.getTagsId() != null) {
                specification = specification.and(buildSpecification(criteria.getTagsId(),
                    root -> root.join(Items_.tags, JoinType.LEFT).get(Tags_.id)));
            }
            if (criteria.getReferencesId() != null) {
                specification = specification.and(buildSpecification(criteria.getReferencesId(),
                    root -> root.join(Items_.references, JoinType.LEFT).get(References_.id)));
            }
        }
        return specification;
    }
}
