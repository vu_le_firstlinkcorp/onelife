package com.onelife.gtd.service;

import com.onelife.gtd.service.dto.ReferencesDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.onelife.gtd.domain.References}.
 */
public interface ReferencesService {

    /**
     * Save a references.
     *
     * @param referencesDTO the entity to save.
     * @return the persisted entity.
     */
    ReferencesDTO save(ReferencesDTO referencesDTO);

    /**
     * Get all the references.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ReferencesDTO> findAll(Pageable pageable);


    /**
     * Get the "id" references.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ReferencesDTO> findOne(Long id);

    /**
     * Delete the "id" references.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
