package com.onelife.gtd.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.onelife.gtd.domain.FeedBack} entity. This class is used
 * in {@link com.onelife.gtd.web.rest.FeedBackResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /feed-backs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FeedBackCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter context;

    private StringFilter status;

    private LocalDateFilter feedDate;

    private LocalDateFilter fixDate;

    public FeedBackCriteria(){
    }

    public FeedBackCriteria(FeedBackCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.context = other.context == null ? null : other.context.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.feedDate = other.feedDate == null ? null : other.feedDate.copy();
        this.fixDate = other.fixDate == null ? null : other.fixDate.copy();
    }

    @Override
    public FeedBackCriteria copy() {
        return new FeedBackCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getContext() {
        return context;
    }

    public void setContext(StringFilter context) {
        this.context = context;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public LocalDateFilter getFeedDate() {
        return feedDate;
    }

    public void setFeedDate(LocalDateFilter feedDate) {
        this.feedDate = feedDate;
    }

    public LocalDateFilter getFixDate() {
        return fixDate;
    }

    public void setFixDate(LocalDateFilter fixDate) {
        this.fixDate = fixDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FeedBackCriteria that = (FeedBackCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(context, that.context) &&
            Objects.equals(status, that.status) &&
            Objects.equals(feedDate, that.feedDate) &&
            Objects.equals(fixDate, that.fixDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        context,
        status,
        feedDate,
        fixDate
        );
    }

    @Override
    public String toString() {
        return "FeedBackCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (context != null ? "context=" + context + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (feedDate != null ? "feedDate=" + feedDate + ", " : "") +
                (fixDate != null ? "fixDate=" + fixDate + ", " : "") +
            "}";
    }

}
