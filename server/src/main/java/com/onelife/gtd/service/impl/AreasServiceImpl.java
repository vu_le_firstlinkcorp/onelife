package com.onelife.gtd.service.impl;

import com.onelife.gtd.service.AreasService;
import com.onelife.gtd.domain.Areas;
import com.onelife.gtd.repository.AreasRepository;
import com.onelife.gtd.service.dto.AreasDTO;
import com.onelife.gtd.service.mapper.AreasMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Areas}.
 */
@Service
@Transactional
public class AreasServiceImpl implements AreasService {

    private final Logger log = LoggerFactory.getLogger(AreasServiceImpl.class);

    private final AreasRepository areasRepository;

    private final AreasMapper areasMapper;

    public AreasServiceImpl(AreasRepository areasRepository, AreasMapper areasMapper) {
        this.areasRepository = areasRepository;
        this.areasMapper = areasMapper;
    }

    /**
     * Save a areas.
     *
     * @param areasDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AreasDTO save(AreasDTO areasDTO) {
        log.debug("Request to save Areas : {}", areasDTO);
        Areas areas = areasMapper.toEntity(areasDTO);
        areas = areasRepository.save(areas);
        return areasMapper.toDto(areas);
    }

    /**
     * Get all the areas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AreasDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Areas");
        return areasRepository.findAll(pageable)
            .map(areasMapper::toDto);
    }


    /**
     * Get one areas by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AreasDTO> findOne(Long id) {
        log.debug("Request to get Areas : {}", id);
        return areasRepository.findById(id)
            .map(areasMapper::toDto);
    }

    /**
     * Delete the areas by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Areas : {}", id);
        areasRepository.deleteById(id);
    }
}
