package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.ReferencesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link References} and its DTO {@link ReferencesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReferencesMapper extends EntityMapper<ReferencesDTO, References> {


    @Mapping(target = "items", ignore = true)
    @Mapping(target = "removeItems", ignore = true)
    @Mapping(target = "projecs", ignore = true)
    @Mapping(target = "removeProjecs", ignore = true)
    References toEntity(ReferencesDTO referencesDTO);

    default References fromId(Long id) {
        if (id == null) {
            return null;
        }
        References references = new References();
        references.setId(id);
        return references;
    }
}
