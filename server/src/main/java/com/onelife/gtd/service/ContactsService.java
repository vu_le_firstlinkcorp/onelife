package com.onelife.gtd.service;

import com.onelife.gtd.service.dto.ContactsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.onelife.gtd.domain.Contacts}.
 */
public interface ContactsService {

    /**
     * Save a contacts.
     *
     * @param contactsDTO the entity to save.
     * @return the persisted entity.
     */
    ContactsDTO save(ContactsDTO contactsDTO);

    /**
     * Get all the contacts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ContactsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" contacts.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ContactsDTO> findOne(Long id);

    /**
     * Delete the "id" contacts.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
