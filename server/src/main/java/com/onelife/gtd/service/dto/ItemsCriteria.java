package com.onelife.gtd.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.onelife.gtd.domain.Items} entity. This class is used
 * in {@link com.onelife.gtd.web.rest.ItemsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /items?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ItemsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter toDo;

    private StringFilter notes;

    private StringFilter time;

    private StringFilter energy;

    private LocalDateFilter dueDate;

    private StringFilter itemType;

    private StringFilter focus;

    private StringFilter status;

    private LongFilter contactsId;

    private LongFilter areasId;

    private LongFilter projecsId;

    private LongFilter tagsId;

    private LongFilter referencesId;

    public ItemsCriteria(){
    }

    public ItemsCriteria(ItemsCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.toDo = other.toDo == null ? null : other.toDo.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.time = other.time == null ? null : other.time.copy();
        this.energy = other.energy == null ? null : other.energy.copy();
        this.dueDate = other.dueDate == null ? null : other.dueDate.copy();
        this.itemType = other.itemType == null ? null : other.itemType.copy();
        this.focus = other.focus == null ? null : other.focus.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.contactsId = other.contactsId == null ? null : other.contactsId.copy();
        this.areasId = other.areasId == null ? null : other.areasId.copy();
        this.projecsId = other.projecsId == null ? null : other.projecsId.copy();
        this.tagsId = other.tagsId == null ? null : other.tagsId.copy();
        this.referencesId = other.referencesId == null ? null : other.referencesId.copy();
    }

    @Override
    public ItemsCriteria copy() {
        return new ItemsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getToDo() {
        return toDo;
    }

    public void setToDo(StringFilter toDo) {
        this.toDo = toDo;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public StringFilter getTime() {
        return time;
    }

    public void setTime(StringFilter time) {
        this.time = time;
    }

    public StringFilter getEnergy() {
        return energy;
    }

    public void setEnergy(StringFilter energy) {
        this.energy = energy;
    }

    public LocalDateFilter getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateFilter dueDate) {
        this.dueDate = dueDate;
    }

    public StringFilter getItemType() {
        return itemType;
    }

    public void setItemType(StringFilter itemType) {
        this.itemType = itemType;
    }

    public StringFilter getFocus() {
        return focus;
    }

    public void setFocus(StringFilter focus) {
        this.focus = focus;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public LongFilter getContactsId() {
        return contactsId;
    }

    public void setContactsId(LongFilter contactsId) {
        this.contactsId = contactsId;
    }

    public LongFilter getAreasId() {
        return areasId;
    }

    public void setAreasId(LongFilter areasId) {
        this.areasId = areasId;
    }

    public LongFilter getProjecsId() {
        return projecsId;
    }

    public void setProjecsId(LongFilter projecsId) {
        this.projecsId = projecsId;
    }

    public LongFilter getTagsId() {
        return tagsId;
    }

    public void setTagsId(LongFilter tagsId) {
        this.tagsId = tagsId;
    }

    public LongFilter getReferencesId() {
        return referencesId;
    }

    public void setReferencesId(LongFilter referencesId) {
        this.referencesId = referencesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ItemsCriteria that = (ItemsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(toDo, that.toDo) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(time, that.time) &&
            Objects.equals(energy, that.energy) &&
            Objects.equals(dueDate, that.dueDate) &&
            Objects.equals(itemType, that.itemType) &&
            Objects.equals(focus, that.focus) &&
            Objects.equals(status, that.status) &&
            Objects.equals(contactsId, that.contactsId) &&
            Objects.equals(areasId, that.areasId) &&
            Objects.equals(projecsId, that.projecsId) &&
            Objects.equals(tagsId, that.tagsId) &&
            Objects.equals(referencesId, that.referencesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        toDo,
        notes,
        time,
        energy,
        dueDate,
        itemType,
        focus,
        status,
        contactsId,
        areasId,
        projecsId,
        tagsId,
        referencesId
        );
    }

    @Override
    public String toString() {
        return "ItemsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (toDo != null ? "toDo=" + toDo + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (time != null ? "time=" + time + ", " : "") +
                (energy != null ? "energy=" + energy + ", " : "") +
                (dueDate != null ? "dueDate=" + dueDate + ", " : "") +
                (itemType != null ? "itemType=" + itemType + ", " : "") +
                (focus != null ? "focus=" + focus + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (contactsId != null ? "contactsId=" + contactsId + ", " : "") +
                (areasId != null ? "areasId=" + areasId + ", " : "") +
                (projecsId != null ? "projecsId=" + projecsId + ", " : "") +
                (tagsId != null ? "tagsId=" + tagsId + ", " : "") +
                (referencesId != null ? "referencesId=" + referencesId + ", " : "") +
            "}";
    }

}
