package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.ProjecsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Projecs} and its DTO {@link ProjecsDTO}.
 */
@Mapper(componentModel = "spring", uses = {ItemsMapper.class, TagsMapper.class, ReferencesMapper.class})
public interface ProjecsMapper extends EntityMapper<ProjecsDTO, Projecs> {

    @Mapping(source = "items.id", target = "itemsId")
    ProjecsDTO toDto(Projecs projecs);

    @Mapping(source = "itemsId", target = "items")
    @Mapping(target = "contacts", ignore = true)
    @Mapping(target = "removeContacts", ignore = true)
    @Mapping(target = "areas", ignore = true)
    @Mapping(target = "removeAreas", ignore = true)
    @Mapping(target = "removeTags", ignore = true)
    @Mapping(target = "removeReferences", ignore = true)
    Projecs toEntity(ProjecsDTO projecsDTO);

    default Projecs fromId(Long id) {
        if (id == null) {
            return null;
        }
        Projecs projecs = new Projecs();
        projecs.setId(id);
        return projecs;
    }
}
