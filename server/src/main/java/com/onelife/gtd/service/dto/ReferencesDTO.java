package com.onelife.gtd.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.References} entity.
 */
public class ReferencesDTO implements Serializable {

    private Long id;

    private String name;

    private String notes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReferencesDTO referencesDTO = (ReferencesDTO) o;
        if (referencesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), referencesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReferencesDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
