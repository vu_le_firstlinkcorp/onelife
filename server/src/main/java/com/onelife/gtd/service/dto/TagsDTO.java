package com.onelife.gtd.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.Tags} entity.
 */
public class TagsDTO implements Serializable {

    private Long id;

    private String context;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TagsDTO tagsDTO = (TagsDTO) o;
        if (tagsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tagsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TagsDTO{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            "}";
    }
}
