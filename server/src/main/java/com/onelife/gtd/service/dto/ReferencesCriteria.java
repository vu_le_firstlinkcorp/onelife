package com.onelife.gtd.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.onelife.gtd.domain.References} entity. This class is used
 * in {@link com.onelife.gtd.web.rest.ReferencesResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /references?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ReferencesCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter notes;

    private LongFilter itemsId;

    private LongFilter projecsId;

    public ReferencesCriteria(){
    }

    public ReferencesCriteria(ReferencesCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.itemsId = other.itemsId == null ? null : other.itemsId.copy();
        this.projecsId = other.projecsId == null ? null : other.projecsId.copy();
    }

    @Override
    public ReferencesCriteria copy() {
        return new ReferencesCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public LongFilter getItemsId() {
        return itemsId;
    }

    public void setItemsId(LongFilter itemsId) {
        this.itemsId = itemsId;
    }

    public LongFilter getProjecsId() {
        return projecsId;
    }

    public void setProjecsId(LongFilter projecsId) {
        this.projecsId = projecsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReferencesCriteria that = (ReferencesCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(itemsId, that.itemsId) &&
            Objects.equals(projecsId, that.projecsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        notes,
        itemsId,
        projecsId
        );
    }

    @Override
    public String toString() {
        return "ReferencesCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (itemsId != null ? "itemsId=" + itemsId + ", " : "") +
                (projecsId != null ? "projecsId=" + projecsId + ", " : "") +
            "}";
    }

}
