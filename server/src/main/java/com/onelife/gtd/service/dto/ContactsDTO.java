package com.onelife.gtd.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.Contacts} entity.
 */
public class ContactsDTO implements Serializable {

    private Long id;

    private String name;


    private Long itemsId;

    private Long projecsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getItemsId() {
        return itemsId;
    }

    public void setItemsId(Long itemsId) {
        this.itemsId = itemsId;
    }

    public Long getProjecsId() {
        return projecsId;
    }

    public void setProjecsId(Long projecsId) {
        this.projecsId = projecsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactsDTO contactsDTO = (ContactsDTO) o;
        if (contactsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contactsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContactsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", items=" + getItemsId() +
            ", projecs=" + getProjecsId() +
            "}";
    }
}
