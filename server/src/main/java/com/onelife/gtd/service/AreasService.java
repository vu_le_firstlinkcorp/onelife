package com.onelife.gtd.service;

import com.onelife.gtd.service.dto.AreasDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.onelife.gtd.domain.Areas}.
 */
public interface AreasService {

    /**
     * Save a areas.
     *
     * @param areasDTO the entity to save.
     * @return the persisted entity.
     */
    AreasDTO save(AreasDTO areasDTO);

    /**
     * Get all the areas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AreasDTO> findAll(Pageable pageable);


    /**
     * Get the "id" areas.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AreasDTO> findOne(Long id);

    /**
     * Delete the "id" areas.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
