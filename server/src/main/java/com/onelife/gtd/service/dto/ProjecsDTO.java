package com.onelife.gtd.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.Projecs} entity.
 */
public class ProjecsDTO implements Serializable {

    private Long id;

    private String name;

    private String notes;

    private LocalDate dueDate;

    private String projectType;

    private String focus;

    private String status;


    private Long itemsId;

    private Set<TagsDTO> tags = new HashSet<>();

    private Set<ReferencesDTO> references = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getItemsId() {
        return itemsId;
    }

    public void setItemsId(Long itemsId) {
        this.itemsId = itemsId;
    }

    public Set<TagsDTO> getTags() {
        return tags;
    }

    public void setTags(Set<TagsDTO> tags) {
        this.tags = tags;
    }

    public Set<ReferencesDTO> getReferences() {
        return references;
    }

    public void setReferences(Set<ReferencesDTO> references) {
        this.references = references;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjecsDTO projecsDTO = (ProjecsDTO) o;
        if (projecsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), projecsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProjecsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", notes='" + getNotes() + "'" +
            ", dueDate='" + getDueDate() + "'" +
            ", projectType='" + getProjectType() + "'" +
            ", focus='" + getFocus() + "'" +
            ", status='" + getStatus() + "'" +
            ", items=" + getItemsId() +
            "}";
    }
}
