package com.onelife.gtd.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.onelife.gtd.domain.Areas} entity.
 */
public class AreasDTO implements Serializable {

    private Long id;

    private String name;


    private Long itemsId;

    private Long projecsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getItemsId() {
        return itemsId;
    }

    public void setItemsId(Long itemsId) {
        this.itemsId = itemsId;
    }

    public Long getProjecsId() {
        return projecsId;
    }

    public void setProjecsId(Long projecsId) {
        this.projecsId = projecsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AreasDTO areasDTO = (AreasDTO) o;
        if (areasDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), areasDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AreasDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", items=" + getItemsId() +
            ", projecs=" + getProjecsId() +
            "}";
    }
}
