package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.AreasDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Areas} and its DTO {@link AreasDTO}.
 */
@Mapper(componentModel = "spring", uses = {ItemsMapper.class, ProjecsMapper.class})
public interface AreasMapper extends EntityMapper<AreasDTO, Areas> {

    @Mapping(source = "items.id", target = "itemsId")
    @Mapping(source = "projecs.id", target = "projecsId")
    AreasDTO toDto(Areas areas);

    @Mapping(source = "itemsId", target = "items")
    @Mapping(source = "projecsId", target = "projecs")
    Areas toEntity(AreasDTO areasDTO);

    default Areas fromId(Long id) {
        if (id == null) {
            return null;
        }
        Areas areas = new Areas();
        areas.setId(id);
        return areas;
    }
}
