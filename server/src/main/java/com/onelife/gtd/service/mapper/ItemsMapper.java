package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.ItemsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Items} and its DTO {@link ItemsDTO}.
 */
@Mapper(componentModel = "spring", uses = {TagsMapper.class, ReferencesMapper.class})
public interface ItemsMapper extends EntityMapper<ItemsDTO, Items> {


    @Mapping(target = "contacts", ignore = true)
    @Mapping(target = "removeContacts", ignore = true)
    @Mapping(target = "areas", ignore = true)
    @Mapping(target = "removeAreas", ignore = true)
    @Mapping(target = "projecs", ignore = true)
    @Mapping(target = "removeProjecs", ignore = true)
    @Mapping(target = "removeTags", ignore = true)
    @Mapping(target = "removeReferences", ignore = true)
    Items toEntity(ItemsDTO itemsDTO);

    default Items fromId(Long id) {
        if (id == null) {
            return null;
        }
        Items items = new Items();
        items.setId(id);
        return items;
    }
}
