package com.onelife.gtd.service.mapper;

import com.onelife.gtd.domain.*;
import com.onelife.gtd.service.dto.ContactsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Contacts} and its DTO {@link ContactsDTO}.
 */
@Mapper(componentModel = "spring", uses = {ItemsMapper.class, ProjecsMapper.class})
public interface ContactsMapper extends EntityMapper<ContactsDTO, Contacts> {

    @Mapping(source = "items.id", target = "itemsId")
    @Mapping(source = "projecs.id", target = "projecsId")
    ContactsDTO toDto(Contacts contacts);

    @Mapping(source = "itemsId", target = "items")
    @Mapping(source = "projecsId", target = "projecs")
    Contacts toEntity(ContactsDTO contactsDTO);

    default Contacts fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contacts contacts = new Contacts();
        contacts.setId(id);
        return contacts;
    }
}
