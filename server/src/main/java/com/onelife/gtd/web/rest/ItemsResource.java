package com.onelife.gtd.web.rest;

import com.onelife.gtd.service.ItemsService;
import com.onelife.gtd.web.rest.errors.BadRequestAlertException;
import com.onelife.gtd.service.dto.ItemsDTO;
import com.onelife.gtd.service.dto.ItemsCriteria;
import com.onelife.gtd.service.ItemsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.onelife.gtd.domain.Items}.
 */
@RestController
@RequestMapping("/api")
public class ItemsResource {

    private final Logger log = LoggerFactory.getLogger(ItemsResource.class);

    private static final String ENTITY_NAME = "items";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ItemsService itemsService;

    private final ItemsQueryService itemsQueryService;

    public ItemsResource(ItemsService itemsService, ItemsQueryService itemsQueryService) {
        this.itemsService = itemsService;
        this.itemsQueryService = itemsQueryService;
    }

    /**
     * {@code POST  /items} : Create a new items.
     *
     * @param itemsDTO the itemsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new itemsDTO, or with status {@code 400 (Bad Request)} if the items has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/items")
    public ResponseEntity<ItemsDTO> createItems(@RequestBody ItemsDTO itemsDTO) throws URISyntaxException {
        log.debug("REST request to save Items : {}", itemsDTO);
        if (itemsDTO.getId() != null) {
            throw new BadRequestAlertException("A new items cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ItemsDTO result = itemsService.save(itemsDTO);
        return ResponseEntity.created(new URI("/api/items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /items} : Updates an existing items.
     *
     * @param itemsDTO the itemsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated itemsDTO,
     * or with status {@code 400 (Bad Request)} if the itemsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the itemsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/items")
    public ResponseEntity<ItemsDTO> updateItems(@RequestBody ItemsDTO itemsDTO) throws URISyntaxException {
        log.debug("REST request to update Items : {}", itemsDTO);
        if (itemsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ItemsDTO result = itemsService.save(itemsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, itemsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /items} : get all the items.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of items in body.
     */
    @GetMapping("/items")
    public ResponseEntity<List<ItemsDTO>> getAllItems(ItemsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Items by criteria: {}", criteria);
        Page<ItemsDTO> page = itemsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /items/count} : count all the items.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/items/count")
    public ResponseEntity<Long> countItems(ItemsCriteria criteria) {
        log.debug("REST request to count Items by criteria: {}", criteria);
        return ResponseEntity.ok().body(itemsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /items/:id} : get the "id" items.
     *
     * @param id the id of the itemsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the itemsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/items/{id}")
    public ResponseEntity<ItemsDTO> getItems(@PathVariable Long id) {
        log.debug("REST request to get Items : {}", id);
        Optional<ItemsDTO> itemsDTO = itemsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(itemsDTO);
    }

    /**
     * {@code DELETE  /items/:id} : delete the "id" items.
     *
     * @param id the id of the itemsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/items/{id}")
    public ResponseEntity<Void> deleteItems(@PathVariable Long id) {
        log.debug("REST request to delete Items : {}", id);
        itemsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
