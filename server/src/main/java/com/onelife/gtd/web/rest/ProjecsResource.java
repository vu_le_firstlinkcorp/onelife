package com.onelife.gtd.web.rest;

import com.onelife.gtd.service.ProjecsService;
import com.onelife.gtd.web.rest.errors.BadRequestAlertException;
import com.onelife.gtd.service.dto.ProjecsDTO;
import com.onelife.gtd.service.dto.ProjecsCriteria;
import com.onelife.gtd.service.ProjecsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.onelife.gtd.domain.Projecs}.
 */
@RestController
@RequestMapping("/api")
public class ProjecsResource {

    private final Logger log = LoggerFactory.getLogger(ProjecsResource.class);

    private static final String ENTITY_NAME = "projecs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProjecsService projecsService;

    private final ProjecsQueryService projecsQueryService;

    public ProjecsResource(ProjecsService projecsService, ProjecsQueryService projecsQueryService) {
        this.projecsService = projecsService;
        this.projecsQueryService = projecsQueryService;
    }

    /**
     * {@code POST  /projecs} : Create a new projecs.
     *
     * @param projecsDTO the projecsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new projecsDTO, or with status {@code 400 (Bad Request)} if the projecs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/projecs")
    public ResponseEntity<ProjecsDTO> createProjecs(@RequestBody ProjecsDTO projecsDTO) throws URISyntaxException {
        log.debug("REST request to save Projecs : {}", projecsDTO);
        if (projecsDTO.getId() != null) {
            throw new BadRequestAlertException("A new projecs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProjecsDTO result = projecsService.save(projecsDTO);
        return ResponseEntity.created(new URI("/api/projecs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /projecs} : Updates an existing projecs.
     *
     * @param projecsDTO the projecsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated projecsDTO,
     * or with status {@code 400 (Bad Request)} if the projecsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the projecsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/projecs")
    public ResponseEntity<ProjecsDTO> updateProjecs(@RequestBody ProjecsDTO projecsDTO) throws URISyntaxException {
        log.debug("REST request to update Projecs : {}", projecsDTO);
        if (projecsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProjecsDTO result = projecsService.save(projecsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, projecsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /projecs} : get all the projecs.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of projecs in body.
     */
    @GetMapping("/projecs")
    public ResponseEntity<List<ProjecsDTO>> getAllProjecs(ProjecsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Projecs by criteria: {}", criteria);
        Page<ProjecsDTO> page = projecsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /projecs/count} : count all the projecs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/projecs/count")
    public ResponseEntity<Long> countProjecs(ProjecsCriteria criteria) {
        log.debug("REST request to count Projecs by criteria: {}", criteria);
        return ResponseEntity.ok().body(projecsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /projecs/:id} : get the "id" projecs.
     *
     * @param id the id of the projecsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the projecsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/projecs/{id}")
    public ResponseEntity<ProjecsDTO> getProjecs(@PathVariable Long id) {
        log.debug("REST request to get Projecs : {}", id);
        Optional<ProjecsDTO> projecsDTO = projecsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(projecsDTO);
    }

    /**
     * {@code DELETE  /projecs/:id} : delete the "id" projecs.
     *
     * @param id the id of the projecsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/projecs/{id}")
    public ResponseEntity<Void> deleteProjecs(@PathVariable Long id) {
        log.debug("REST request to delete Projecs : {}", id);
        projecsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
