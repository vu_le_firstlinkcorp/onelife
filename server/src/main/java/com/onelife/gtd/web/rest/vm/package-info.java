/**
 * View Models used by Spring MVC REST controllers.
 */
package com.onelife.gtd.web.rest.vm;
