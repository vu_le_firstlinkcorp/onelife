package com.onelife.gtd.web.rest;

import com.onelife.gtd.service.AreasService;
import com.onelife.gtd.web.rest.errors.BadRequestAlertException;
import com.onelife.gtd.service.dto.AreasDTO;
import com.onelife.gtd.service.dto.AreasCriteria;
import com.onelife.gtd.service.AreasQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.onelife.gtd.domain.Areas}.
 */
@RestController
@RequestMapping("/api")
public class AreasResource {

    private final Logger log = LoggerFactory.getLogger(AreasResource.class);

    private static final String ENTITY_NAME = "areas";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AreasService areasService;

    private final AreasQueryService areasQueryService;

    public AreasResource(AreasService areasService, AreasQueryService areasQueryService) {
        this.areasService = areasService;
        this.areasQueryService = areasQueryService;
    }

    /**
     * {@code POST  /areas} : Create a new areas.
     *
     * @param areasDTO the areasDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new areasDTO, or with status {@code 400 (Bad Request)} if the areas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/areas")
    public ResponseEntity<AreasDTO> createAreas(@RequestBody AreasDTO areasDTO) throws URISyntaxException {
        log.debug("REST request to save Areas : {}", areasDTO);
        if (areasDTO.getId() != null) {
            throw new BadRequestAlertException("A new areas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AreasDTO result = areasService.save(areasDTO);
        return ResponseEntity.created(new URI("/api/areas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /areas} : Updates an existing areas.
     *
     * @param areasDTO the areasDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated areasDTO,
     * or with status {@code 400 (Bad Request)} if the areasDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the areasDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/areas")
    public ResponseEntity<AreasDTO> updateAreas(@RequestBody AreasDTO areasDTO) throws URISyntaxException {
        log.debug("REST request to update Areas : {}", areasDTO);
        if (areasDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AreasDTO result = areasService.save(areasDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, areasDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /areas} : get all the areas.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of areas in body.
     */
    @GetMapping("/areas")
    public ResponseEntity<List<AreasDTO>> getAllAreas(AreasCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Areas by criteria: {}", criteria);
        Page<AreasDTO> page = areasQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /areas/count} : count all the areas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/areas/count")
    public ResponseEntity<Long> countAreas(AreasCriteria criteria) {
        log.debug("REST request to count Areas by criteria: {}", criteria);
        return ResponseEntity.ok().body(areasQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /areas/:id} : get the "id" areas.
     *
     * @param id the id of the areasDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the areasDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/areas/{id}")
    public ResponseEntity<AreasDTO> getAreas(@PathVariable Long id) {
        log.debug("REST request to get Areas : {}", id);
        Optional<AreasDTO> areasDTO = areasService.findOne(id);
        return ResponseUtil.wrapOrNotFound(areasDTO);
    }

    /**
     * {@code DELETE  /areas/:id} : delete the "id" areas.
     *
     * @param id the id of the areasDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/areas/{id}")
    public ResponseEntity<Void> deleteAreas(@PathVariable Long id) {
        log.debug("REST request to delete Areas : {}", id);
        areasService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
