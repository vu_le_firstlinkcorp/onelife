package com.onelife.gtd.web.rest;

import com.onelife.gtd.service.ContactsService;
import com.onelife.gtd.web.rest.errors.BadRequestAlertException;
import com.onelife.gtd.service.dto.ContactsDTO;
import com.onelife.gtd.service.dto.ContactsCriteria;
import com.onelife.gtd.service.ContactsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.onelife.gtd.domain.Contacts}.
 */
@RestController
@RequestMapping("/api")
public class ContactsResource {

    private final Logger log = LoggerFactory.getLogger(ContactsResource.class);

    private static final String ENTITY_NAME = "contacts";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactsService contactsService;

    private final ContactsQueryService contactsQueryService;

    public ContactsResource(ContactsService contactsService, ContactsQueryService contactsQueryService) {
        this.contactsService = contactsService;
        this.contactsQueryService = contactsQueryService;
    }

    /**
     * {@code POST  /contacts} : Create a new contacts.
     *
     * @param contactsDTO the contactsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactsDTO, or with status {@code 400 (Bad Request)} if the contacts has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contacts")
    public ResponseEntity<ContactsDTO> createContacts(@RequestBody ContactsDTO contactsDTO) throws URISyntaxException {
        log.debug("REST request to save Contacts : {}", contactsDTO);
        if (contactsDTO.getId() != null) {
            throw new BadRequestAlertException("A new contacts cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactsDTO result = contactsService.save(contactsDTO);
        return ResponseEntity.created(new URI("/api/contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contacts} : Updates an existing contacts.
     *
     * @param contactsDTO the contactsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactsDTO,
     * or with status {@code 400 (Bad Request)} if the contactsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contacts")
    public ResponseEntity<ContactsDTO> updateContacts(@RequestBody ContactsDTO contactsDTO) throws URISyntaxException {
        log.debug("REST request to update Contacts : {}", contactsDTO);
        if (contactsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactsDTO result = contactsService.save(contactsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, contactsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contacts} : get all the contacts.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contacts in body.
     */
    @GetMapping("/contacts")
    public ResponseEntity<List<ContactsDTO>> getAllContacts(ContactsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Contacts by criteria: {}", criteria);
        Page<ContactsDTO> page = contactsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /contacts/count} : count all the contacts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/contacts/count")
    public ResponseEntity<Long> countContacts(ContactsCriteria criteria) {
        log.debug("REST request to count Contacts by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contacts/:id} : get the "id" contacts.
     *
     * @param id the id of the contactsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contacts/{id}")
    public ResponseEntity<ContactsDTO> getContacts(@PathVariable Long id) {
        log.debug("REST request to get Contacts : {}", id);
        Optional<ContactsDTO> contactsDTO = contactsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactsDTO);
    }

    /**
     * {@code DELETE  /contacts/:id} : delete the "id" contacts.
     *
     * @param id the id of the contactsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contacts/{id}")
    public ResponseEntity<Void> deleteContacts(@PathVariable Long id) {
        log.debug("REST request to delete Contacts : {}", id);
        contactsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
